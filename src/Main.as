package 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GroupedProperties;
	import bb.game.AppConfig;
	import bb.game.components.CounterBubble;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.engine.env.components.properties.LinearMovement;
	import flash.events.MouseEvent;
	//import fl.controls.TextArea;
	//import fl.controls.TextInput;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	
	import tests.*;	//All tests
	/**
	 * ...
	 * @author CruzBalm Games
	 */
	public class Main extends Sprite 
	{

		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			configApp();
			// entry point
			
			//testAng();
			
			//TestTheLiveLongMode
			testLiveLongMode();
			
			//Test the diry bubblr
			//testDirtyBubble();
			
			//Test Counter Bubble Bursting
			//testBordersAndReflect();
			
			//Test Counter Bubble Bursting
			//testCounterBubble();
			
			//Test a regular bubble
			//testRegularBubble();
			
			//Test Grouped properties
			//testGroupProperties();
			
			//Test OmniBurst functionality
			//testOmniBurst();
			
			//Test CounterBubbleMergeThenPop
			//testCounterBubbleMergeThenPop();
						
			//Test BasicBubblePop
			//testBasicBubblePop();
			
			//Test CounterBubbleMerge
			//testCounterBubbleMerge();
			
			//Testing LinearMovement Functionality
			//testLinearMovement();
			
			//bubble and borders
			//testBubbleAndBorders();
		}
		
		private function testAng():void {
			trace(Math.atan(1 / 1));
			trace(Math.atan(1 / 0));
			var rad = Math.atan2( -1, -1);
			trace("RAD " + rad);
			
			var ang = ((rad / 3.14) * 180)+360;
			trace("ANG " + ang);
			
			var startPoint:Point = new Point(670, -10);
			var q1:Point = new Point(790, 10);
			
			var adely:Number = q1.y - startPoint.y;
			var adelx:Number = q1.x - startPoint.x;
			
			var maxAngle:Number = Math.atan2(adely * -1, adelx);
			
			
			
		}
		
		private function testLiveLongMode():void {
			new TestLiveLongMode(this).test();
		}
		
		private function testDirtyBubble():void {
			new TestDirtyBubble(this).test();
		}
		
		private function testBordersAndReflect():void {
			new TestBordersAndReflect(this).test();
		}
		
		private function testRegularBubble():void {
			new TestRegularBubble(this).test();
		}
		
		private function testCounterBubble():void {
			new TestCounterBubble(this).test();
		}
		
		private function testGroupProperties():void {
			new TestGroupedProperties(this).test();
		}
		
		private function testOmniBurst():void {
			new TestOmniBurst(this).test();
		}
		
		private function testLinearMovement():void 
		{	
			new TestLinearMovement(this).test();	
		}
		
		private function testBubbleAndBorders():void
		{
			new TestBubblesAndBorders(this).test();
		}
		
		private function testCounterBubbleMerge():void 
		{
			new TestCounterBubbleMerge(this).test();
		}
		
		private function testBasicBubblePop():void {
			new TestBasicBubblePop(this).test();
		}
		
		private function testCounterBubbleMergeThenPop():void {
			new TestCounterBubbleMergeThenPop(this).test();
		}
		
		private function configApp():void {
			//Set the root layer
			AppConfig.setRootLayer(this);
		}
	}
	
}