package bb.engine.env.components 
{
	import adobe.utils.ProductManager;
	import flash.display.Sprite;
	import bb.engine.env.components.properties.IEnvObjProperty;
	
	/**
	 * EnvObj represents an Environment Object.  Fundamental piece of the BB Framework.
	 * All objects that require self aware interaction should derive from this class.
	 * Maintains a reference to a property manager instance in which controls the 
	 * properties that are applied to this EnvObj.
	 * ...
	 * @author Rich Balmir
	 */
	public class EnvObj extends Sprite 
	{
		private var propManager:PropertyManager = null;
		
		public function EnvObj() 
		{
			super();
		}
		
		/**
		 * All Environment objects should be clonable and should be implemented by derived class
		 * @return
		 */
		public function clone():EnvObj {
			propManager = null;	//reset this property manager
			return null;
		}
		
		/**
		 * Remove all resources allocated for this enviroment object
		 */
		public function destroy(preservePM:Boolean = false):void {
			
			if (this.parent != null)
				this.parent.removeChild(this);
			
			/*
			if(propManager != null || preservePM == true){
				propManager.destroyProperties();
				propManager = null;
			}
			*/
			
		}
		
		/**
		 * Set the property manager that handle the properties
		 * @param	pM
		 */
		public function setPropertyManager(pM:PropertyManager):void {
			//!!Should deactivate old pM and activate new one.
			if (propManager != null)
				propManager.deactiveProperties()
			
			propManager = pM;
		}
		
		public function getPropertyManager():PropertyManager {
			if (propManager == null)
				propManager = new PropertyManager(this);
				
			return propManager;
		}
		
	}

}