package bb.engine.env.components 
{
	import flash.utils.Dictionary;
	/**Maintains a class owned global table of environmental objects.
	 * All clients can use this to maintain their objects for any use
	 * to make available to other parts of the system.
	 * 
	 * Client must make use of proper ID to maintain or make use of 
	 * stored EnvObjs.
	 * 
	 * ...
	 * @author Rich Balmir
	 */
	public class GlobalEnvObjTable 
	{
		private var globalEnvObjTable:Dictionary;
		static private var instance:GlobalEnvObjTable = null;
		
		public function GlobalEnvObjTable(enf:Enforcer) 
		{
			globalEnvObjTable = new Dictionary();
		}
		
		/**
		 * Get instance of GlobalEnvObjTableHandler
		 * @return
		 */
		static public function getInstance():GlobalEnvObjTable {
			
			if (instance == null) {
				instance = new GlobalEnvObjTable(new Enforcer());
			}
			
			return instance;
		}
		
		/**
		 * Get a set of Env Objs
		 * @param	id
		 * @return
		 */
		public function getEnvObjs(id:String):Array {
			var outArr:Array = null;
			
			if (id in globalEnvObjTable)
				outArr = globalEnvObjTable[id];
			
			return outArr;
		}
		
		/**
		 * Add Env Obj to the table
		 * @param	id
		 * @param	envObj
		 */
		public function addEnvObj(id:String, envObj:EnvObj):void {
			
			if (id in globalEnvObjTable) {
				(globalEnvObjTable[id] as Array).push(envObj);
			}else {
			
				var tempArr:Array = new Array();
				tempArr.push(envObj);
				globalEnvObjTable[id] = tempArr;			
			}
		}
		
		public function addEnvObjs(id:String, envObjs:Array):void {
			
			for each(var envObj:EnvObj in envObjs) {
				addEnvObj(id, envObj);
			}
			
		}
		
		/**
		 * Test for existence of ID
		 * @param	id
		 * @return Returns set of Env Objs
		 */
		public function exists(id:String):Boolean {

			if (id in globalEnvObjTable){
				return true;
			}else 
				return false;
		}
		
		public function removeEnvObj(id:String, obj:EnvObj):void {
			
			if(obj != null){
				if (exists(id)) {
					var arr:Array = globalEnvObjTable[id];
					var idx:int = arr.indexOf(obj);
					if(idx > -1)
						arr.splice(idx,1);
				}
			}
		}
		
		
		public function destroyObjectsViaPM():void {
			
			for each(var id:String in globalEnvObjTable) {
				var envObjs:Array = globalEnvObjTable[id];
				
				for each(var envObj:EnvObj in envObjs) {
					envObj.destroy();
				}
				
			}
		}
		
		/**
		 * Remove a set of Env Objs
		 * @param	id
		 */
		public function removeEnvObjSet(id:String):void {
			if(id in globalEnvObjTable){
				globalEnvObjTable[id] = null;
				delete globalEnvObjTable[id];
			}
		}
		
	}	
	
}

internal class Enforcer{}