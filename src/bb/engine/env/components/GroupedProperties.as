/*
//TODO Add Exception Handling to methods
*/
package bb.engine.env.components
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.properties.EnvObjProperty;
	import bb.engine.env.components.properties.IInvokableEnvObjProperty;
	import bb.engine.env.components.properties.InvokableEnvObjProperty;
	/**
	 * Group Properties help manage the grouping and chaining of env obj properties.
	 * As a new properties are attached to group they will be autowired to the last
	 * entered property.
	 * wired: [prop] -> [prop] -> [prop]
	 * unwired: {[prop], [prop], [prop]}
	 * wired tree: [prop] -> [prop] -> [prop]
	 * 								-> [grouped property]
	 * Allows the building of a wired tree.  You can set another group of properties to be notified
	 * to the last wired property in the group.  
	 * 
	 * This class removes the boiler plate code required to set target env objects to
	 * a property, chainging properties, activating, deactivating and 
	 * destroying properties appropriately.
	 * 
	 * @author ...
	 */
	public class GroupedProperties 
	{
		private var desc:String = null;
		private var wiredArray:Array = null;
		private var unwiredArray:Array = null;
		private var targetEnvObj:EnvObj = null;
		//FYI:in makeGroup method. 
		//Defined an Object struct internally to manage each item in group.
		//property: envObjProp,		//EnvObj Property
		//linkedGroups: null		//Array of Linked Groups to this node that get notified
		
		/**
		 * Initialize the Group Properties with a description of what the group
		 * of properties are intended to do.
		 * @param	description
		 */
		public function GroupedProperties(description:String = "none") 
		{
			desc = description;
			
			wiredArray = new Array();
			unwiredArray = new Array();
		}
		
		/**
		 * Get description of these group of properties
		 * @return
		 */
		public function getDescription():String{
			return desc;
		}
		
		/**
		 * Add a property to the group.  
		 * Default:will autowire the property to the last property entered.
		 * Can also have property placed in non wired set. This is for properties 
		 * that need to be activated/deactivated collectively that may do not require 
		 * to be wired to the last property entered.
		 * First property added to group is not wired to anything.
		 * @param	envObjProp
		 * @param	autoWire
		 */
		public function addProperty(envObjProp:InvokableEnvObjProperty, autoWire:Boolean = true):void{
			
			if(envObjProp != null){
				
				if (autoWire) {
					
					if (wiredArray == null)
						wiredArray = new Array();
					
					wiredArray.push(makeGroupItem(envObjProp));
					autoWireProp(this.wiredArray);
					
				}else {
					if (unwiredArray == null)
						unwiredArray = new Array();
					
					unwiredArray.push(makeGroupItem(envObjProp));
				}
				
			}else
				throw new Error("GroupProperties: Added null property");
			
		}	
		
		/**
		 * Returns array of properties by a classification ID(PROJECTABLE, DESTROYABLE, etc).
		 * Can return all properties in group when passed null
		 * @param	id
		 * @return
		 */
		public function getProperties(propTypeID:String = null):Array {
			
			if(propTypeID != null){
				//In scope delegate: find matching property types
				var func:Function = function(groupItem:*, idx:int, arr:Array):Boolean {
					if((groupItem.property as InvokableEnvObjProperty).getPropertyID() == propTypeID)
						return true;
				return false;
				}
				
				return parseOutPropsToArr(wiredArray.concat(unwiredArray).filter(func));
			}else
				return parseOutPropsToArr(wiredArray.concat(unwiredArray));
				
		}
		
		/**
		 * Activate all the properties in the group
		 */
		public function activate():void {
			//send as one array
			var aggregation:Array = new Array();
			
			if (unwiredArray != null)
				aggregation = aggregation.concat(unwiredArray);
			
			if (wiredArray != null)
				aggregation = aggregation.concat(wiredArray);
			
			performActivation(aggregation);	
		}
		
		/**
		 * Deactivate all the properties in the group
		 */
		public function deactivate():void {
			var aggregation:Array = new Array();
			
			if (unwiredArray != null)
				aggregation = aggregation.concat(unwiredArray);
			
			if (wiredArray != null)
				aggregation = aggregation.concat(wiredArray);
			
			performDeactivation(aggregation);
		}
		
		/**
		 * Set the target Env Obj for group of properties to work on.
		 * Note: This will deactivate properties for a previous set env obj target.
		 * @param	envObj
		 */
		public function setTargetEnvObj(envObj:EnvObj):void {
			
			if (targetEnvObj != envObj) {
				
				if(targetEnvObj != null)
					deactivate();
			
					targetEnvObj = envObj;	
			}
		}
		
		/**
		 * Set another GroupedProperties to be notified.
		 * Links to the last property in group.
		 * @param	groupToLink
		 */
		public function setLinkedGroup(groupToLink:GroupedProperties):void {
			//check to see not linking to itself
			if (groupToLink == this)
				throw new Error("GroupedProperties:setLinkedGroup = attempted to link group to itself ");
			
			//Get last index of this group properties
			if (wiredArray.length > 0) {
				//Get last item in this group to set a link to it
				var lastGroupItem:Object = wiredArray[wiredArray.length--];
			
				//get first item in group to link
				var firstGroupItemToLinkTo:Object = getFirstGroupItem(groupToLink); 
				
				//Wire the groups together
				wireGroupItemPair(lastGroupItem, firstGroupItemToLinkTo);
				
				//init linked Groups array
				if (lastGroupItem.linkedGroups == null)
					lastGroupItem.linkedGroups = new Array();
					
				//Store group to link to 
				(lastGroupItem.linkedGroups as Array).push(groupToLink);
			}
			
		}
		
		/**
		 * Destroy this Group of Properties.
		 * This implies that each property is deactivated, properties are unwired from each 
		 * other to be GC.
		 */
		public function destroy():void {
			//deactivate
			deactivate();
			
			//for each unwire
			unwireGroupProperties();
			
			unwiredArray = null;
			wiredArray = null;
		}
		
		//get the first group item out of a Group of Properties 
		private function getFirstGroupItem(groupedProperties:GroupedProperties):Object {
			
			var groupItem:Object = null;
			
			if (groupedProperties.getProperties().length >= 1)
				groupedProperties.getProperties()[0];
			
			return groupItem;
		}
		
		//parses out solely envObjProperties from an array of group Items
		private function parseOutPropsToArr(groupItems:Array):Array {
			var justProps:Array = new Array();
			
			//Retrieve the property from each groupIten
			groupItems.forEach(function(groupItem:*, ind:int, arr:Array):void {
				justProps.push(groupItem.property);
			});
			
			return justProps;
		}
		
		//Make a group item object to insert into grouped properties array
		private function makeGroupItem(envObjProp:InvokableEnvObjProperty = null):Object {
			var o:Object = {
				property: envObjProp,	//EnvObj Property
				linkedGroups: null		//Array of Linked Groups
				};
				
			return o;
		}
		
		//Activate each property in group
		private function performActivation(arr:Array):void {
			
			//in line delegate
			arr.forEach(function(groupItem:*, idx:int, array:Array):void {
				var prop:InvokableEnvObjProperty = (groupItem.property as InvokableEnvObjProperty);
				
				if (!prop.isActive()) {
					prop.setEnvObj(targetEnvObj);
					prop.activate();
				}
			});
		}
		
		//deactivate each property in group
		private function performDeactivation(arr:Array):void {
			
			//in line delegate
			arr.forEach(function(groupItem:*, idx:int, array:Array):void {
				var prop:InvokableEnvObjProperty = (groupItem.property as InvokableEnvObjProperty);
				
				if (prop.isActive()){
					prop.deactivate();
				}
			});
		}
		
		//Auto wires the last added element in group property
		private function autoWireProp(groupItemArr:Array):void {
			
			if (groupItemArr.length >= 2){
				//Get the property in the last index out of the groupItem
				var child:Object = groupItemArr[groupItemArr.length - 1];
				var parent:Object = groupItemArr[groupItemArr.length - 2];		
				
				//Wire Last Prop to Previous Prop			
				wireGroupItemPair(parent,child);
			}
		}
		
		//wire group Item to each other.. linked list fashion
		private function wireGroupItemPair(parentGroupItem:Object, childGroupItem:Object):void {
			
			if(parentGroupItem.property != null && childGroupItem.property != null)
				parentGroupItem.property.addEventListener(parentGroupItem.property.getPropertyID(), childGroupItem.property.invokeProperty);
			else
				throw new("Attempted to wire null object parent:" + parentGroupItem.property + " child:" + childGroupItem.property);
		}
		
		//unwire group Items from each other
		private function unwireGroupItemPair(parentGroupItem:Object, childGroupItem:Object):void {
			
			
			if(parentGroupItem.property != null && childGroupItem.property != null)
				parentGroupItem.property.removeEventListener(parentGroupItem.property.getPropertyID(), childGroupItem.property.invokeProperty);
			else
				throw new("Attempted to unwire null object parent:" + parentGroupItem.property + " child:" + childGroupItem.property);
		}
		
		//unwire the entire group properties
		private function unwireGroupProperties():void {
			
			this.wiredArray.concat(unwiredArray).forEach(
				function(elem:*, idx:int, arr:Array):void {
					
					//only if not last groupItem unwire child
					if(idx != arr.length - 1)
						unwireGroupItemPair(elem,arr[idx + 1]);
					
					//unwire linked properties	
					unwireLinkedGroup(elem);
				})
		}
		
		//unwire linked groups
		private function unwireLinkedGroup(groupItem:Object):void {
			
			if (groupItem.linkedGroups != null) {
					var currProp:EnvObjProperty;
					
					//for each linked GroupedProperty
					(groupItem.linkedGroups as Array).forEach(
						
						function(elem:*, idx:int, arr:Array):void {
							//For the current groupItem
							currProp = groupItem.property;
							
							//remove listener
							currProp.removeEventListener(currProp.getPropertyID(), 
								(getFirstGroupItem(elem as GroupedProperties).property.invokeProperty));
							
						}
					)
				
				//null the link groups
				groupItem.linkedGroups = null;
			}
			
		}
		
	}

}