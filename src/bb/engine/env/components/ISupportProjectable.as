package bb.engine.env.components 
{
	import bb.engine.env.components.properties.IProjectable;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public interface ISupportProjectable 
	{
		function getIProjectable():IProjectable;
	}
	
}