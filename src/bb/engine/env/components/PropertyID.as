package bb.engine.env.components 
{
	/**
	 * Enumerations of the Property Classifications.
	 * ...
	 * @author ...
	 */
	public class PropertyID 
	{
		public static const CLOCKABLE:PropertyID = new PropertyID("CLOCKABLE");
		public static const COLLISIONABLE:PropertyID = new PropertyID("COLLISIONABLE");
		public static const DESTROYABLE:PropertyID = new PropertyID("DESTROYABLE");
		public static const MERGEABLE:PropertyID = new PropertyID("MERGEABLE");
		public static const MULTIPLYABLE:PropertyID = new PropertyID("MULTIPLYABLE");
		public static const PROJECTABLE:PropertyID = new PropertyID("PROJECTABLE");
		public static const TOUCHABLE:PropertyID = new PropertyID("TOUCHABLE");
		public static const TRANSFORMABLE:PropertyID = new PropertyID("TRANSFORMABLE");
		public static const MISCABLE:PropertyID = new PropertyID("MISCABLE");		//MISCELLANEOUS
		
		private var id:String = "";
		
		public function PropertyID(id:String)
		{	this.id = id;	}
		
		public function toString():String 
		{	return id;	}
		
	}

}