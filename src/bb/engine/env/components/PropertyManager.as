package bb.engine.env.components 
{
	import flash.utils.Dictionary;
	import bb.engine.support.GUID;
	/**
	 * ...
	 * Facility to manage set of grouped properties for an Env Object.
	 * Can choose to active and deactive grouped properties by key name identifiers.
	 * @author ...
	 */
	public class PropertyManager 
	{
		private var propTable:Dictionary; //Table with all the registered grouped properties
		private var targetEnvObj:EnvObj;	//Environment object in which managed properties 
																			//will be applied to.
		
		/**
		 * Initialize the PropertyManager with the target Environment Object in which it will
		 * manage grouped properties for.
		 * @param	targetEnvObj
		 */
		public function PropertyManager(targetEnvObj:EnvObj) 
		{
			if (targetEnvObj == null)
				throw new Error("PropertyManager:ctor - null targetEnvObj set");
			
			propTable = new Dictionary();
			this.targetEnvObj = targetEnvObj;
		}
		
		/**
		 * Get a string out of all the grouped properties registered to this property manager.
		 * String contains list of <Key(GroupedProperty Identifier), GroupedProperty Description>
		 * @return
		 */
		public function getKeyList():String {
			var print:String = "";
			for (var key:String in propTable) {
				print = print + key + ":" + (propTable[key] as GroupedProperties).getDescription() + "\n";
			}
			
			return print;
		}
		
		public function setTargetEnvObj(targetEnvObj:EnvObj):void {
			
			//deactivate
			
			//for each set target object for each grouped properties
			
		}
		
		/**
		 * Activates specific or all grouped properties in manager
		 * Can active a group of properties by the key identier
		 * Use null parameter to activate all grouped properties in manager
		 * @param	key
		 * @return
		 */
		public function activateProperties(key:String = null):Boolean{
			
			var wasHandled:Boolean = false;
			
			//find the id in the dictionary
			if (key in propTable){
				(propTable[key] as GroupedProperties).activate();
				wasHandled = true;
			
			}else if (key == null){
				//Activate each group in dictionary
				for each(var gp:GroupedProperties in propTable)
					gp.activate();
					
				wasHandled = true;
			}
			
			return wasHandled;
		}
		
		/**
		 * Deactive specific or all groupedProperties in manager
		 * Can deactivate a group of properties by the key identier
		 * Use null parameter to deactivate all grouped properties in manager
		 * @param	key
		 * @return
		 */
		public function deactiveProperties(key:String = null):Boolean {
			
			var wasHandled:Boolean = false;
			
			if (key in propTable) {
				(propTable[key] as GroupedProperties).deactivate();
				wasHandled = true;
				
			}else if (key == null) {
				//Deactivate all
				for each(var gp:GroupedProperties in propTable)
					gp.deactivate();
					
				wasHandled = true;
			}
			
			return wasHandled;
		}
		
		/**
		 * Destroy a specific groupedProperties or all.
		 * @param	key
		 * @return
		 */
		public function destroyProperties(key:String = null):Boolean{
			var wasHandled:Boolean = false;
			
			if (key in propTable) {
				(propTable[key] as GroupedProperties).destroy();
				delete propTable[k];
				wasHandled = true;
				
			}else if (key == null) {
				//Destroy all
				for(var k:String in propTable) {
					(propTable[k] as GroupedProperties).destroy();
					
					//delete it from the table
					delete propTable[k];
				}
				wasHandled = true;
			}
			
			return wasHandled;
		}
		
		/**
		 * Attach a group of properties to this manager.  
		 * Supply a key to reference the grouped properties.
		 * If no key supplied, guid will be generated as key.
		 * @param	groupedProps
		 * @param	key
		 * @return
		 */
		public function attachProperties(groupedProps:GroupedProperties, key:String = null):Boolean {
			
			var wasAttached:Boolean = false;
			
			if (key == null)
				key = GUID.create();
			
			if (groupedProps == null)
				throw new Error("Property Mananger:attachProperties - attempted to attach ");
			
			if (key in propTable == false){
				groupedProps.setTargetEnvObj(targetEnvObj);
				propTable[key] = groupedProps;
				wasAttached = true;
			}
			else
				throw new Error("Property Mananger:attachProperties - key:" + key + " already exists");
				
				return wasAttached;
		}
		
		/**
		 * Retrieves a grouped properties from manager. 
		 * This removes grouped property from manager and deactivates the group.  
		 * Use when wanting to reuse the grouped properties for another env object.
		 */
		public function retrieveProperties(key:String):GroupedProperties {
			var groupedProps:GroupedProperties = null;
			
			if (key in propTable){
				//get the
				groupedProps = propTable[key];
				//set target to null..note this auto calls deactivate
				groupedProps.setTargetEnvObj(null);
				
				delete propTable[key]
			}
			
			return groupedProps;
		}
	}

}