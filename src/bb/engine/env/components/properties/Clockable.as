/*            
Application: BB Object Interaction Framework

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
*/
package bb.engine.env.components.properties 
{
	/**
	 * Clockable used to allows other properties to be notified of deterministic or non-deterministic
	 * clock.  Derived classes defines how clock ticks are determined.
	 * @author Rich Balmir
	 */
	public class Clockable extends InvokableEnvObjProperty 
	{
		
		public function Clockable() 
		{
			super();
			this.propertyID = "CLOCKABLE";
		}
				
	}

}