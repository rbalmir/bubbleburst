/*            
Application: BB Object Interaction Framework

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
//TODO 1)Should check if obj reference does not already exist in array before adding
//TODO 2)Should only do collision detection if property activated
*/

package bb.engine.env.components.properties 
{
	import bb.engine.env.components.EnvObj;
	//import flash.events.Event;
	//import flash.events.IEventDispatcher;
	import bb.engine.env.components.properties.InvokableEnvObjProperty;
	/**
	 * Handles collision detection against the a set of env objects.
	 * Wire to another property to indicate when a collision should be checked.
	 * 
	 * @example Wire to clock events - On each clock tick then collision will be checked.
	 * 
	 * @author Balm Games
	 */
	public class Collisionable extends InvokableEnvObjProperty
	{
		
				//The projection handler to redirect target Env Obj after collision
		protected var collidableEnvObjs:Array = new Array();	//Env Objs to check collision against
		
		public function Collisionable() 
		{
			super();
			this.propertyID = "COLLISIONABLE";
		}
	
		/**
		 * Set the projection handler used to manipulate target env obj projection/orientation
		 * @param	projector
		 */
		
		 /*
		public function setProjectorHandler(projector:IProjectable):void {
			//set a stored IProjectable		
			storedIProjectable = projector;
		}
		*/
		
		/**
		 * set an environment object to check collisions against
		 * @param	envObj
		 */
		public function setWatchEnvObj(envObj:EnvObj):void {
			//Add the border to a border array	
			collidableEnvObjs.push(envObj);
		}
		
		/**
		 * set array environment objects to check collisions against
		 * @param	envObjs
		 */
		public function setWatchEnvObjs(envObjs:Array):void {
			//Add the array of borders to the border array
			collidableEnvObjs = envObjs;
		}
			
		/**
		 * Check for collision and broadcast if collision occured
		 */
		override protected function propertyInvoked():void {
							
				for each(var envObj:EnvObj in collidableEnvObjs) {
						
					if (this.envObj.hitTestObject(envObj)) {
						
						//bubble up notification
						super.propertyInvoked();
						break;
					}	
				}
		}
		
		/**
		 * Retrieve an array of currently colliding objects.
		 * This would be used when a client object is aggregating the collision property.
		 * @example If  property(A) registers to a collision event, then property(A) can 
		 * aggregate the collision to retrieve the colliding objects and manipulate those.
		 * @return Array<EnvObj>
		 */
		public function getCollidingEnvObjs():Array {
			
			var overlappedObjs:Array = null;
			
			for each(var envObj:EnvObj in collidableEnvObjs) {
					
				if (this.envObj.hitTestObject(envObj) && (this.envObj != envObj)) {
					
					if (overlappedObjs == null)
						overlappedObjs = new Array();
					
					overlappedObjs.push(envObj);
				}
				
			}
						
			return overlappedObjs;
		}
		
	}

}