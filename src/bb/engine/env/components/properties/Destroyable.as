/*            
Application: BB Object Interaction Framework

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
*/
package bb.engine.env.components.properties 
{
	/**
	 * Non-Abstract class for all destroyable objects
	 * @author Rich Balmir
	 */
	public class Destroyable extends Transformable 
	{
		
		public function Destroyable() 
		{
			super();
			this.propertyID = "DESTROYABLE";
		}
		
		
	}

}