package bb.engine.env.components.properties 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class EnterFrameClock extends Clockable 
	{
		//Used to register arbitraty object to retrieve enter frame events 
		private var tempSpr:Sprite = null;
		//var clock:Timer;
		
		public function EnterFrameClock() 
		{
			super();
		}
		
		/**
		 * Begin broadcasting enter frame events
		 */
		override public function activate():void 
		{
			if (tempSpr == null) {
				tempSpr = new Sprite();
				tempSpr.addEventListener(Event.ENTER_FRAME,this.invokeProperty);
			}
			
			/*
			clock = new Timer(1000 / 25, 0);
			clock.addEventListener(TimerEvent.TIMER, this.invokeProperty);
			clock.start();
			*/
			
			//Base handles setting active member
			super.activate();
		}
		
		/**
		 * Remove
		 */
		override public function deactivate():void 
		{
			
			//remove the added listener to disable
			try{
				tempSpr.removeEventListener(Event.ENTER_FRAME,this.invokeProperty);
				tempSpr = null;
			}catch (e:Error) {
				trace(e.message);
			}
			
			//clock.removeEventListener(TimerEvent.TIMER, this.invokeProperty);
			
			//Base handles setting active member
			super.deactivate();
		}
		
	}

}