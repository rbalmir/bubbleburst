package bb.engine.env.components.properties 
{
	import bb.engine.env.components.EnvObj;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/**
	 * Abstract management of property.  Property is used to define a characteristic
	 * to be applied to a env obj.
	 * @author BalmFX
	 */
	public class EnvObjProperty extends EventDispatcher implements IEnvObjProperty 
	{	
		protected var envObj:EnvObj = null;			//Env Obj instance property is targeting
		protected var propertyID:String = "";		//Classification ID of the property
		private var activeState:Boolean = false;	
		
		public function EnvObjProperty() 
		{
			super();
		}
		
		/* INTERFACE env.components.properties.IEnvObjProperty */
		/**
		 * Activate the property
		 * Derived class must call
		 */
		public function activate():void 
		{	activeState = true;
		}
		/**
		 * Deactivate the property
		 * Derived class must call
		 */
		public function deactivate():void 
		{	activeState = false;
		}
		
		/**
		 * Returns status if property is currently active
		 * @return
		 */
		public function isActive():Boolean {
			return activeState;
		}
		
		/**
		 * Set the target environment for which the property should work on
		 * @param	envObj
		 */
		public function setEnvObj(envObj:EnvObj):void 
		{
			this.envObj = envObj;
		}
		
		public function getEnvObj():EnvObj 
		{
			return this.envObj;
		}
		
		
		
		/**
		 * Get the Classification ID of the property
		 * @return
		 */
		public function getPropertyID():String 
		{
			return this.propertyID;
		}
		
		/**
		 * Intended to be used as a unique identifier for an instance of a property
		 * @return
		 */
		public function getPropertyHandlerID():String 
		{
			throw new Error();
		}
		
		/**
		 * Dispatchs: Notifies subscribers of event when property performed it work on 
		 * the target env obj.  Passes the classification ID to the event.
		 */
		protected function propertyInvoked():void {
			//only if this property is active should we broadcast
			//prevents work around from activating and deactivating
			if(isActive())
				dispatchEvent(new Event(propertyID));	
		}
		
		
	}

}