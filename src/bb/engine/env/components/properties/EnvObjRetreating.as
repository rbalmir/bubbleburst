package bb.engine.env.components.properties 
{
	import bb.game.components.Bubble;
	import flash.geom.Point;
	/**
	 * Determines if obj is moving away from a defining reference point.
	 * @author ...
	 */
	public class EnvObjRetreating extends Miscable 
	{
		var referencePoint:Point;
		var previousMag:Number;
	
		var firstPass:Boolean = true;
		
		public function EnvObjRetreating(referencePoint:Point) 
		{
			super();
			this.referencePoint = referencePoint;
		}
		
		
		override protected function propertyInvoked():void {
		
			if (firstPass) {
				firstPass = false;
			}else {
					
				var currentMag:Number = calcDistanceToReferencePoint();
				
				//trace("[EnvObjRetreating] BUBID:" + (this.envObj as Bubble).getGUID() + " Prev:" + previousMag + " Curr:" + currentMag);
				
				if(currentMag > previousMag){ //Indicates retreating from point
					super.propertyInvoked();
				}
					
			}
				
			previousMag = calcDistanceToReferencePoint();
		}
		
		
		private function calcDistanceToReferencePoint():Number {

			var xdiff:Number = this.envObj.x - this.referencePoint.x;
			var ydiff:Number = this.envObj.y - this.referencePoint.y;
			return Math.sqrt(Math.pow(xdiff,2) + Math.pow(ydiff,2));	
		}
		
		public function setReferencePoint(refPt:Point) {
			this.referencePoint.x = refPt.x;
			this.referencePoint.y = refPt.y;
		}
		
		public function getReferencePoint():Point {
			return new Point(referencePoint.x, referencePoint.y);
		}
		
	}

}