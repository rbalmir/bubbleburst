/*
GlobalCollisionable.as                                                                  
Language:       AS3            
Application:                   

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
*/
package bb.engine.env.components.properties 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GlobalEnvObjTable;
	
	import flash.utils.Dictionary;
	
	/**
	 * Class Operation:
	 * Used to access to test collision against env obj in a global manner
	 * * @author Rich Balmir
	 */
	public class GlobalCollisionCheck extends Collisionable 
	{
		private var envObjCollectionID:String;
		
		/**
		 * Check the id of objects to check against
		 * @param	id of objects to check against in the GlobalEnvObjTable
		 */
		public function GlobalCollisionCheck(envObjCollectionID:String) 
		{
			super();
			setEnvObjCollectionID(envObjCollectionID);
		}
		
		//evaluate collection id for set we are testing coliisions against
		public function getEnvObjCollectionID():String {
			return envObjCollectionID;
		}
		
		//Set the collection to test against
		public function setEnvObjCollectionID(id:String):void {
			
			//Set and get the array of Env Objs
			this.setWatchEnvObjs(
					GlobalEnvObjTable.getInstance().getEnvObjs(id));
			
			envObjCollectionID = id;
		}			
		
	}

}