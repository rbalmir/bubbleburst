package bb.engine.env.components.properties 
{
	/**
	 * ...
	 * @author ...
	 */
	public class GlobalReflectOnCollision extends GlobalCollisionCheck 
	{
		
		public function GlobalReflectOnCollision(envObjCollectionID:String) 
		{
			super(envObjCollectionID);
			
		}
		
				/**
		 * Check for collision and broadcast if collision occured
		 */
		override protected function propertyInvoked():void {
							
				for each(var envObj:EnvObj in collidableEnvObjs) {
						
					if (this.envObj.hitTestObject(envObj)) {
						
						//bubble up notification
						super.propertyInvoked();
						super.super.propertyInvoked();
						break;
					}	
				}
		}
		
	}

}