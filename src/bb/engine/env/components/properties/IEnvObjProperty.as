package bb.engine.env.components.properties 
{
	import bb.engine.env.components.EnvObj;

	/**
	 * Describes interface for properties that can be applied to 
	 * environmental objects.
	 * ...
	 * @author BalmFX
	 */
	public interface IEnvObjProperty 
	{
		function activate():void;
		function deactivate():void;
		function setEnvObj(envObj:EnvObj):void;
		function getPropertyID():String;
		function getPropertyHandlerID():String;
	}
	
}