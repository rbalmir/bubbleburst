package bb.engine.env.components.properties 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public interface IInvokableEnvObjProperty 
	{
		function invokeProperty(event:Event = null):void;
	}
	
}