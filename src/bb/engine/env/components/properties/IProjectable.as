package bb.engine.env.components.properties 
{
	import flash.geom.Point;
	
	/**
	 * Allows a client to retrieve a Projection control from an
	 * Env Obj if the Env Obj intends on explicitly supporting 
	 * the property.
	 * @author Balm Games
	 */
	public interface IProjectable 
	{
		function setProjection(ang:Number, mag:Number):void; 
		function setPosition(point:Point):void; 
		function getProjectionAngle():Number;
		function getProjectionMag():Number;	
	}
	
}