package bb.engine.env.components.properties 
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	/**
	 * Fix:6/25/11 Invoke property appears to be calling base twice, force duplicate notifications when only one notification
	 * should go out
	 * @author Rich Balmir
	 */
	public class InvokableEnvObjProperty extends EnvObjProperty implements IInvokableEnvObjProperty 
	{		
		public function InvokableEnvObjProperty() 
		{
			super();
		}
		
		/* INTERFACE bb.engine.env.components.properties.IInvokableEnvObjProperty */		
		/**
		 * @
		 */
		public function invokeProperty(event:Event = null):void 
		{	
			//This would call the base as it is not implement here
			this.propertyInvoked();		//Make base class known that property invoked to broadcast notification
		}			
	}
}