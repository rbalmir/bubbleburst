/*
LinearMovement.as                                                                  
Language:       AS3            
Application:                   

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
	 * Maintainance:
	 * RB Issue: angle_x and angle_y calculated would round to zero since int.
	 * RB Fix: Change class variables radians, angle_x, angle_y changed to type :Number. Previously type :Int.
	 * RB Issue: Would bubble up notifications on zero movement projection
	 * RB FIx: Test for update value of x and y equal to 0
*/

package bb.engine.env.components.properties
{
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * Class Operation:
	 * Performs linear project for object
	 * 
	 * @author Balm Games
	 */
	public class LinearMovement extends Projectable
	{
		//Note:You can access the envObj sprite use: this.envObj;
		//private var radians:Number;
		private var angle_x:Number = 0;
		private var angle_y:Number = 0;
		
		public function LinearMovement() 
		{
			super();	
		}
		
		//Ang in degrees:Set the magnitude and direction of the envObj. 
		//Mag will be in pixels.
		public override function setProjection(ang:Number, mag:Number):void 
		{
				/*
				this.storedAng = ang;
				this.storedMag = mag;
				var radians:Number = degree2radian(ang);
				angle_x = Math.sin(radians) * mag;
				angle_y = Math.cos(radians) * mag;
				angle_y *= -1;
				*/
				
				this.storedAng = ang;
				this.storedMag = mag;
		}
		
		//Set the initialize 
		public override function setPosition(point:Point):void 
		{
			//new Point
			this.envObj.x = point.x;
			this.envObj.y = point.y;
			
		}
		
		//This will be called every time the next envObj position should
		//be calculated
		override protected function propertyInvoked():void
		{
			//!!Update
			//Only if env Obj position changes should you dispatch event
			//if (angle_x != 0 && angle_y != 0) {
			if (this.storedMag != 0){
				//Update the env obj position
				//Should be recalculating
				var radians:Number = Projectable.degToRad(this.storedAng);
				
				var dely:Number = Math.sin(radians) * this.storedMag;
				var delx:Number = Math.cos(radians) * this.storedMag;
				
				this.envObj.x += delx;
				this.envObj.y += (dely * -1);
				
				//this bubbles up to notify subscribers that this Env Obj has moved
				super.propertyInvoked();
			}
		}
		
		//add private functions here
		private function degree2radian (deg:Number):Number
		{
			return deg * (Math.PI / 180);
		}
		
	}

}