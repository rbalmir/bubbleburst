package bb.engine.env.components.properties 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * Empty class just sets the property id
	 * @author Rich Balmir
	 */
	public class Mergeable extends Transformable 
	{
		
		public function Mergeable() 
		{
			super();
			this.propertyID = "MERGEABLE";
		}
		
		
		
	}

}