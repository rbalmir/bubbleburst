/*            
Application: BB Object Interaction Framework

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
*/
package bb.engine.env.components.properties 
{
	import bb.engine.env.components.PropertyID;
	/**
	 * Clockable used to allows other properties to be notified of deterministic or non-deterministic
	 * clock.  Derived classes defines how clock ticks are determined.
	 * @author Rich Balmir
	 */
	public class Miscable extends InvokableEnvObjProperty 
	{
		
		public function Miscable() 
		{
			super();
			this.propertyID = "MISCABLE";
		}
				
	}

}