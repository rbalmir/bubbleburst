package bb.engine.env.components.properties 
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class MouseClicked extends Touchable 
	{
		
		public function MouseClicked() 
		{
			super();
		}
	
		override public function activate():void 
		{
			//Register this object for mouse click events
			this.envObj.addEventListener(MouseEvent.MOUSE_DOWN, this.invokeProperty);
			super.activate();
		}
		
		//disable this movable property
		override public function deactivate():void 
		{
			//remove the added listener to disable
			this.envObj.removeEventListener(MouseEvent.MOUSE_DOWN, this.invokeProperty);
			super.deactivate();
		}
		
	}

}