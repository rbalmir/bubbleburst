package bb.engine.env.components.properties 
{
	/**
	 * ...
	 * @author ...
	 */
	public class ProjectModable extends InvokableEnvObjProperty 
	{
		/**
		 * Intended to modify a projection on a object
		 */
		private var projectableHandler:Projectable;	
		 
		public function ProjectModable(projectable:Projectable) 
		{
			super();
			setProjectorHandler(projectable);	
		}
		
		public function getProjectorHandler():Projectable {
			return projectableHandler;
		}
		
		public function setProjectorHandler(projector:Projectable):void {
			//set a stored IProjectable		
			this.projectableHandler = projector;
		}
		
		
		
	}

}