/*
Movable.as                                                                  
Language:       AS3            
Application:                     

Build Process                                                    
Required Files:                                                  

Maintenance History
===================
Future implementation.. Can in incorporate movement based on time 
*/

package bb.engine.env.components.properties 
{
	import flash.events.IEventDispatcher;
	import bb.engine.env.components.EnvObj;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * Class Operation:
	 * Provides base functionality to support handle moving and object across screen. Movement across screen is frame based.
	 * 
	 * All derived classes are expected to override the method:
	 * override protected function propertyInvoked():void 
	 * Ensure in the overridden method to call the base implementation
	 * super.propertyInvoked();
	 * 
	 * This is required in order to bubble up
	 * 
	 * @author Balm Games
	 */
	//need to make internal
	public class Projectable extends InvokableEnvObjProperty implements IProjectable
	{
		protected var storedAng:Number = 0;
		protected var storedMag:Number = 0;
	
		public function Projectable() 
		{
			super();
			
			this.propertyID = "PROJECTABLE";
		}
		
		/* INTERFACE bb.components.envObjs.IenvObjProperty */
		
		/**
		 * Convert radians to degrees
		 * @param	rad
		 * @return
		 */
		static public function radToDeg(rad:Number):Number 
		{	return (rad / Math.PI) * 180;	}
		
		//add private functions here
		static public function degToRad(deg:Number):Number
		{	return deg * (Math.PI / 180);	}
		
		//add the listener needed to support moving the envObj across the screen
		//this would be enter frame events
		public override function activate():void 
		{
			setPosition(new Point(this.envObj.x, this.envObj.y));
			super.activate();
		}
		
		//Get Angle
		public function getProjectionAngle():Number {
			return storedAng;
		}
		//Get Magnitude
		public function getProjectionMag():Number {
			
			return storedMag;
		}
		
		//Set the magnitude and direction of the envObj. 
		//Mag will be in pixels.
		public function setProjection(ang:Number, mag:Number):void 
		{
			//Add code here
			storedAng = ang;
			storedMag = mag;	
		}
		
		//Set the initialize 
		public function setPosition(point:Point):void 
		{	
			this.envObj.x = point.x;
			this.envObj.y = point.y;
		}
		
				//Set the initialize 
		public function getPosition():Point
		{	
			return new Point(this.envObj.x, this.envObj.y);			 
		}
		
	}

}