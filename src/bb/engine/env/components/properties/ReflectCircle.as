package bb.engine.env.components.properties 
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.Bubble;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	/**
	 * //TODO: determineCollidingPoint - improve find detemineColliding Point by analyzing heading angle 
	 * //TODO: High fidelity 
	 * ...
	 * ReflectAng = HitPointAng + 180 - HitPointAng - AngOfAttack
	 * @author ...
	 */
	public class ReflectCircle extends ProjectModable 
	{
		private var collisionHandler:Collisionable;
		
		/**
		 * Provides reflection for circular target objects
		 */
		public function ReflectCircle(projectable:Projectable, collisionable:Collisionable) 
		{
			super(projectable);
			setCollisionHandler(collisionable);
		}
		
		public function setCollisionHandler(collisionable:Collisionable):void {
			collisionHandler = collisionable;
		}
		
		/**
		 * 
		 */
		override protected function propertyInvoked():void {

			//Loop over each colliding object
			var collObjs:Array = collisionHandler.getCollidingEnvObjs();
			
			collObjs.forEach(function(collEnvObj:*, idx:int, array:Array):void {
				
				//Perform reflection on the target
				//var t:Projectable = this.projectableHandler;
				var ts:Projectable = getProjectorHandler();
				doReflect(ts, collEnvObj);
				
			});
			
			if (collObjs.length != 0) 
				super.propertyInvoked(); //Notify
			
		}
		
		//reflect the target off the colliding Obj
		private function doReflect(target:Projectable, collidingObj:EnvObj):void {
			
			var targetObj:EnvObj = target.getEnvObj();
			
			//get to the colliding object
			var targetPos:Point = target.getPosition();
			
			//get the hit test point
			//var collPoint:Point = determineCollidingNormalPoint(target,collidingObj);
			
			//var reflectAng:Number = determineReflectionAng(target, collidingObj);
			var reflectAng:Number = determineReflectionAngBetter(target, collidingObj);
			
			//set the new angle
			target.setProjection(reflectAng, target.getProjectionMag());
		}
		
		private function determineReflectionAng(target:Projectable, collidingObj:EnvObj):Number {
			
			//get circles radius
			var radius:Number = target.getEnvObj().width / 2;
			var radians:Number = 0;
			var computed:Boolean = false;
			//get circles center
			var targetCenter:Point = new Point(target.getEnvObj().x, target.getEnvObj().y);
			
			var angToHitPoint:Number = null;
			
			var pAng:Number = target.getProjectionAngle();
			
			//for each some odd degrees
			for (var deg:Number = (pAng - 45); deg <= (pAng + 45); deg += 5) {
			//for (var deg:Number = 0; deg <= 360; deg += 15) {
				
				//Get each point around circles edge 
				//radians = (2 * Math.PI * deg) / 360;
				radians = Projectable.degToRad(deg);
				
				//Get the x and y difference from center of circle to outer edge of circle
				var plusY:Number = Math.sin(radians) * radius * -1;
				var plusX:Number = (Math.cos(radians) * radius) ;
	
				//generate the test point
				var testPointX:Number = targetCenter.x + plusX;
				var testPointY:Number = targetCenter.y + plusY;
	
				if (collidingObj.hitTestPoint(testPointX, testPointY)) {
					angToHitPoint = deg;
					//trace("ReflectCicrle BUBID:" + (target.getEnvObj() as Bubble).getGUID() + "targetCenter wid:" + target.getEnvObj().width  +  " x:" + targetCenter.x + " y:" + targetCenter.y);
					//trace("ReflectCircle BUBID:" + (target.getEnvObj() as Bubble).getGUID() + "TestPoint x:" + testPointX + " y:" + testPointY + " angToHP:" + deg);
					(target.getEnvObj() as Bubble).getGUID();
					computed = true;
					break;
				}
			}
			
			var outAng:Number = target.getProjectionAngle();
			
			if(computed){
				//Corrects angle to hit point - based on assumed 
				angToHitPoint = dirtyAngleCorrection(angToHitPoint);
				
				//Comput the reflection angle
				outAng = angToHitPoint - 180 + angToHitPoint - target.getProjectionAngle();
			}
			
			return outAng;
		}

		
		private function determineReflectionAngBetter(target:Projectable, collidingObj:EnvObj):Number {
			
			//get circles radius
			var radius:Number = target.getEnvObj().width / 2;
			var radians:Number = 0;
			var computed:Boolean = false;
			//get circles center
			var targetCenter:Point = new Point(target.getEnvObj().x, target.getEnvObj().y);
			
			var angToHitPoint:Number = null;
			var langToHitPoint:Number = null;
			var rangToHitPoint:Number = null;
			
			var pAng:Number = target.getProjectionAngle();
			
			var angInc:Number = 5;
			
			//appproach from right
			for (var deg:Number = (pAng - 90); deg <= (pAng + 90); deg += angInc) {				
				//Get each point around circles edge 
				radians = Projectable.degToRad(deg);
				//Get the x and y difference from center of circle to outer edge of circle
				var plusY:Number = Math.sin(radians) * radius * -1;
				var plusX:Number = (Math.cos(radians) * radius) ;
				//generate the test point
				var testPointX:Number = targetCenter.x + plusX;
				var testPointY:Number = targetCenter.y + plusY;
	
				if (collidingObj.hitTestPoint(testPointX, testPointY)) {
					rangToHitPoint = deg;
					computed = true;
					break;
				}
			}
			//approach from left
			for (var deg:Number = (pAng + 90); deg >= (pAng - 90); deg -= angInc) {				
				//Get each point around circles edge 
				radians = Projectable.degToRad(deg);
				//Get the x and y difference from center of circle to outer edge of circle
				var plusY:Number = Math.sin(radians) * radius * -1;
				var plusX:Number = (Math.cos(radians) * radius) ;
				//generate the test point
				var testPointX:Number = targetCenter.x + plusX;
				var testPointY:Number = targetCenter.y + plusY;
	
				if (collidingObj.hitTestPoint(testPointX, testPointY)) {
					langToHitPoint = deg;
					computed = true;
					break;
				}
			}
			
			var outAng:Number = target.getProjectionAngle();
			
			if(computed){
				angToHitPoint = (langToHitPoint + rangToHitPoint) / 2;
				outAng = angToHitPoint - 180 + angToHitPoint - target.getProjectionAngle();
			}
			
			return outAng;
		}
		
		private function positiveEquivalent(inDeg:Number):Number {
			
				var deg:Number = inDeg;
			
				if (inDeg < 0){
					var mod:Number =  Math.floor(Math.abs(inDeg)/ 360);
				deg += 360 * (mod + 1);
					}else if (inDeg > 360) {
				deg = inDeg - (360 * Math.floor(inDeg / 360));
				}
				
				return deg;
		}
		
		
		//Dirty way to correct angle
		private function dirtyAngleCorrection(inDeg:Number):Number {
			
			var deg:Number = inDeg;
			
			if (inDeg < 0){
				var mod:Number =  Math.floor(Math.abs(inDeg)/ 360);
				deg += 360 * (mod + 1);
			}else if (inDeg > 360) {
				deg = inDeg - (360 * Math.floor(inDeg / 360));
			}
			
			if (deg > 45 && deg <= 135) {
				deg = 90;
				
			}else if (deg > 135 && deg <= 225) {
				deg = 180;
				
			}else if (deg > 225 && deg <= 315) {
				deg = 270;
			}else { 
				deg = 0;
			}
			
			return deg;			
		}
		
		
	}
}