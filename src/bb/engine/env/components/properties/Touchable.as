package bb.engine.env.components.properties 
{
	import bb.engine.env.components.PropertyID;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class Touchable extends InvokableEnvObjProperty 
	{
		
		public function Touchable() 
		{
			super();
			//this.propertyID = "TOUCHABLE";
			this.propertyID = PropertyID.TOUCHABLE.toString();
		}
		
	}

}