package bb.engine.env.components.properties 
{
	import flash.events.IEventDispatcher;
	
	/**
	 * Implements a Chain of Responisibility pattern. If a Transformable type
	 * does not want to handle the invokeProperty request it will pass it onto
	 * the next one.  
	 * Can set member to force each handler in chain to handle property alternatively.
	 * 
	 * @author Rich Balmir
	 */
	public class Transformable extends InvokableEnvObjProperty 
	{
		private var forcedChained:Boolean;
		private var nextTransformer:Transformable = null;
		//private var firstInChain:Boolean = false;
		
		public function Transformable(/*firstInChain:Boolean*/) 
		{
			super();
			this.propertyID = "TRANSFORMABLE";
			//this.firstInChain = firstInChain;
			forcedChained = false;
		}
		
		//Flag to Force processing of all handlers
		public function forceChain(isForced:Boolean):void {
			forcedChained = isForced;
		}
		
		//Set next transformer to invoke
		public function setNextInChain(transformer:Transformable):void{
			this.nextTransformer = transformer;
		}
		
		
		/**
		 * Derived types are required to call this to bubble up the chain of responsibilty
		 * @param	reqHandled
		 */
		public function propertyInvokedAlt(reqHandled:Boolean):void {
				
			//
			if (reqHandled == false){ 
				
				if(nextTransformer != null)
					nextTransformer.propertyInvoked();
				
			}else {
				//Bubble up that propertyInvoked 
				super.propertyInvoked();
				
				//Force next to process
				if (forcedChained == true && nextTransformer != null){
					nextTransformer.forceChain(forcedChained);
					nextTransformer.propertyInvoked();
				}
			}
				
		}
		
		/**
		 * @throws Derived should be calling propertyInvokedAlt instead
		 */
		override protected function propertyInvoked():void {
			//derived types should be calling propertyInvokedAlt of the Transformable
			//base class.
			throw new Error("Transformable Derived Types class should call propertyInvokedAlt(requestHandled:Boolean) instead on Base");		
		}
		
	}	
}