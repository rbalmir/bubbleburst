package bb.game.components 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.properties.IProjectable;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class Bubble extends EnvObj 
	{
		static private var globalIDCounter:int = 0;
		private var globalID:int = 0;
		private var bubbleID:String;
		protected var iProject:IProjectable = null;
		
		/**
		 * Set the bubbles classification id
		 * @param	id
		 */
		public function Bubble(id:String) 
		{
			super();
			bubbleID = id;
			globalID = globalIDCounter++;
		}
		
		public function getGUID():int {
			return globalID;
		}
		
		public function getIProjectable():IProjectable {
			return iProject;
		}
		
		public function setIProjectable(iP:IProjectable):void {
			this.iProject = iP;
		}

		protected function activateAllProperties():void {
			throw new Error("Not Yet Implemented..If");
		}
		
		protected function deactivateAllProperties():void {
			throw new Error("Not Yet Implemented..If");
		}
		
		/**
		 * Destroy the properties.. remove from display list...
		 * @param		e
		 */
		public function dispose(e:Event = null):void{
			super.destroy();
			
			//notify subscribers
			dispatchEvent(new Event("DISPOSEBUBBLE"));
			
			//Destroy properties
			//Error:Base takes care of this in destroy
			if (this.getPropertyManager() != null){
				this.getPropertyManager().destroyProperties();
				this.setPropertyManager(null);
			}
			
		}
		
		/*
		public override function destroy(preservePM:Boolean = false):void {
			dispose();
			
			//Remove from the display list and destroys properties
			super.destroy();
		}
		*/
		
		//protected 
		
		public function getBubbleClassID():String {
			return bubbleID;
		}
		
		protected function addTestBubble(color:uint, radius:uint = 20):Sprite {
			var bub:Sprite = new Sprite();
			bub.graphics.beginFill(color, 1);
			bub.graphics.drawCircle(0, 0, radius);
			bub.graphics.endFill();
			addChild(bub);
			
			return bub;
		}
		
		protected function makeTestBubble(color:uint, radius:uint):Sprite {
			var bub:Sprite = new Sprite();
			bub.graphics.beginFill(color, 1);
			bub.graphics.drawCircle(0, 0, radius);
			//bub.getBounds(
			bub.graphics.endFill();
			
			return bub;
		}
		
		protected function makeTestSquare(color:uint, width:int, height:int):Sprite {
			var sqr:Sprite = new Sprite();
			sqr.graphics.beginFill(color, 1);
			sqr.graphics.drawRect(0, 0, width, height);
			sqr.graphics.endFill();
			
			return sqr;
		}
	}
}