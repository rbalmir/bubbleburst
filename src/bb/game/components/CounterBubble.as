package bb.game.components 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import bb.game.components.properties.BasicBubblePop;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.game.components.properties.CounterBubbleMerge;
	import bb.game.components.properties.OmniBurst;
	import bb.engine.env.components.GlobalEnvObjTable;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.sampler.NewObjectSample;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	/**
	 * Counter Bubble counts upward.  When the bubble is hit.  It will burst into
	 * the current counter value.  When hit on top of the another counter, those 
	 * bubbles will merge and average thier values
	 * @author Rich Balmir
	 */
	public class CounterBubble extends Bubble implements ISupportProjectable
	{
		static private var ID:String = "CounterBubble";
		
		//private var iProject:IProjectable = null;
		private var tfCounter:TextField= null;
		
		private var bubbleBaseColor:uint = 0x333333;
		private var counterCircleColor:uint = 0xFFDDFF;
		
		private var omniBurst:OmniBurst;
		static private var timer:Timer = null;
		
		private var aDestoryBub:BasicBubblePop;
		
		private var counter:int;
		private var counterDelay:int = 2000;
		
		public function CounterBubble() 
		{
			super(ID);
			initialize();
		}
		
		/**
		 * Draw bubble.. set timer counter.. config env properties
		 */
		protected function initialize() {
			//Register this bubble to the global environmental object table
			GlobalEnvObjTable.getInstance().addEnvObj(ID, this);
			
			//make the counter bubble
			drawBubble();
			
			//register to a second interval clockable
			//to handle the counter changing
			if(timer == null)
				timer = new Timer(counterDelay, 10000);
			
			//create the timer
			timer.addEventListener(TimerEvent.TIMER, countEvent);
			timer.start();
			
			//configure
			configProperties();
			
			//activate the bubbles properties
			this.getPropertyManager().activateProperties();
		}
		
		/**
		 * Get the IProjectable for this object
		 * @return
		 */
		/*
		public function getIProjectable():IProjectable {
			return iProject;
		}
		*/
		/**
		 * Get the counters time
		 * @return
		 */
		public function getTime():int {
			//Get time from the count down Property
			return counter;
		}
		
		/**
		 * Set the counters time
		 * @param	modCount
		 */
		public function setTime(modCount:int):void {
			counter = modCount;
		}
		
		/**
		 * Sets
		 * @param	sec
		 */
		public function setCounterDelay(sec:int) {
			counterDelay = sec;
		}
		
		private function drawBubble() {
		
			//create the base of the bubble and add it to this object
			var bubbleBase:Sprite = addTestBubble(0x333333);
			var innerWidth:Number = (bubbleBase.width * .70);
			
			//make the counter circle and add to base circle
			var counterCircle:Sprite = makeTestBubble(counterCircleColor, innerWidth / 2);
			bubbleBase.addChild(counterCircle);
			
			//create text, bound text boundaries to counter circle limits
			tfCounter = new TextField();
			tfCounter.text = "0";
			tfCounter.width = counterCircle.width;
			tfCounter.height = counterCircle.height;
			setTFCounterProperties(tfCounter);
			
			//shift text
			tfCounter.x = tfCounter.x - tfCounter.width / 2;
			tfCounter.y = tfCounter.y - tfCounter.width / 2;
			
			//Add counter text
			counterCircle.addChild(tfCounter);
						
			trace("couterCircleWidth" + counterCircle.width);
			trace("width:" + tfCounter.width + "tfCounter.x:" + tfCounter.x);
			trace("twidth:" + tfCounter.textWidth + "tfCounter.y:" + tfCounter.y);
		}
		
		
		private function setTFCounterProperties(tfCounter:TextField):void {
			
			var tf:TextFormat = new TextFormat(null, 21, null, null, null, null, null, null, TextFormatAlign.CENTER, null, null, null, null);
			//tfCounter.setTextFormat(tf);
			tfCounter.defaultTextFormat = tf;
			tfCounter.textColor = 0x0000dd;
			tfCounter.selectable = false;
			//tfCounter.backgroundColor = 0xAAAAAA;
			//tfCounter.background = true;
			
			/*
			var css:StyleSheet =  new StyleSheet();
			css.
			tfCounter.styleSheet = 
			*/
		}
		
		//Configure the properties that make a CounterBubble
		protected function configProperties():void {
			//1)Handles mouse click, 
			//2)merging of counter bubbles, 
			//3)omniburst into regular bubbles
			//4)and destruction of target bubble.
			//---------------------------------------
			var grpTouch2Transform:GroupedProperties = new GroupedProperties("Touch->MergableooMultiplyable");
						
			//Create Touchable
			var aClick:Touchable = new MouseClicked();
			grpTouch2Transform.addProperty(aClick);
			
			//Collisionable Checker
			var aCollisionable:Collisionable = new GlobalCollisionCheck(getBubbleClassID());
			grpTouch2Transform.addProperty(aCollisionable, false);
			
			//wire Mergable to Touchable 
			var aMerge:Mergeable = new CounterBubbleMerge(aCollisionable);
			grpTouch2Transform.addProperty(aMerge);
			
			
			//chain Multiplyable to Mergable
			this.omniBurst = new OmniBurst();
			this.omniBurst.setProjectableClonableEnvObj(new RegularBubble()); //burst into regular bubbles
			aMerge.setNextInChain(this.omniBurst);
			
			//Add to the property manager
			getPropertyManager().attachProperties(grpTouch2Transform);
			
			//
			//add burst as unwired since chained to mergable
			//grpTouch2Transform.addProperty(omniBurst, false);
			
			var grpBurst2Destroy:GroupedProperties = new GroupedProperties("Multiplyable->Destroyable");
			grpBurst2Destroy.addProperty(omniBurst);

			//wire Destroyable to Multiplyable
			this.aDestoryBub = new BasicBubblePop();
			grpBurst2Destroy.addProperty(this.aDestoryBub);
			
			//Add to the property manager
			getPropertyManager().attachProperties(grpBurst2Destroy);
			
			//!!Note should remove like wise
			//This bubble wants to know when its destroyed
			aDestoryBub.addEventListener(aDestoryBub.getPropertyID(), dispose);
			
			
			//1)On Enter Frame Clock
			//2)Project across stage linearly
			//3)When wall detected, reflect accordingly
			//----------------------------------------
			//Bounce off borders
			var grpTickProjector:GroupedProperties = new GroupedProperties("Clockable->Projectable->ObjRetreating->Collisionable->Reflection");
			
			//Create Clockable
			var tick:Clockable = new EnterFrameClock();
			grpTickProjector.addProperty(tick);
			
				//wire Projectable to clockable
				var project:Projectable = new LinearMovement();
				grpTickProjector.addProperty(project);
			
					//Determines if object is retreating from center of stage
					var centerStage:Point = new Point(StageObjects.getGameLayer().width / 2, StageObjects.getGameLayer().height / 2);
					var objRetreating:EnvObjRetreating = new EnvObjRetreating(centerStage);
					grpTickProjector.addProperty(objRetreating);
					
						//wire Collisionable to Projectable
						var collide:Collisionable = new Collisionable();
						collide.setWatchEnvObjs(StageObjects.getStageBorders());
						grpTickProjector.addProperty(collide);
						
							//reflect off walls when collode
							var reflection:ReflectCircle = new ReflectCircle(project, collide);
							grpTickProjector.addProperty(reflection);
			
			//Set the projectable
			iProject = project;
				
			//Add to the property manager
			getPropertyManager().attachProperties(grpTickProjector);
				
		}
		
		
		private function countEvent(e:Event):void {
			//Increase the counter
			this.counter++;
			
			//update the omniburst property
			this.omniBurst.setBurstCount(this.counter);
			
			//Update the Text on this bubble
			this.tfCounter.text = this.counter.toString();
						
		}
		
		override public function dispose(e:Event = null):void 
		{
			//Remove this bubble
			GlobalEnvObjTable.getInstance().removeEnvObj(getBubbleClassID(), this);			
			
			//destroy all the properties
			getPropertyManager().destroyProperties();
			
			//Remove this obj from the global environment table
			super.dispose(e);
			
			//
			//Counter bubble should be destroyed
		}
		
		override public function clone():EnvObj{
			return new CounterBubble();
		}
		
	}

}