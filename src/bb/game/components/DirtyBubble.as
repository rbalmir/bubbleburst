package bb.game.components 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import flash.display.Sprite;
	import bb.game.components.properties.*;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class DirtyBubble extends Bubble implements ISupportProjectable 
	{
		//private var iProject:IProjectable = null;
		
		public function DirtyBubble() 
		{
			super("DirtyBubble");
			
			initialize();
		}
		
		/* INTERFACE bb.engine.env.components.ISupportProjectable */
		
		/*
		public function getIProjectable():IProjectable 
		{
			return iProject;
		}
		*/
		
		private function initialize():void {
			
			//make the bubble graphic
			drawBubble();
			
			//configure
			configProperties();
			
			//activate the bubbles properties
			this.getPropertyManager().activateProperties();
		}
		
		private function drawBubble() {
		
			//create the base of the bubble and add it to this object
			var bubbleBase:Sprite = addTestBubble(0x000000);
			bubbleBase.alpha = .8;
		}
		
		//Configure the properties that make a CounterBubble
		protected function configProperties():void {
						
			//1)On Enter Frame Clock
			//2)Project across stage linearly
			//3)When wall detected, reflect accordingly
			//----------------------------------------
			var grpTickProjector:GroupedProperties = new GroupedProperties("Clockable->Projectable->Collisionable");
			
			//Create Clockable
			var tick:Clockable = new EnterFrameClock();
			grpTickProjector.addProperty(tick);
			
			//wire Projectable to clockable
			var project:Projectable = new LinearMovement();
			grpTickProjector.addProperty(project);
				
					//Determines if object is retreating from center of stage
					var centerStage:Point = new Point(StageObjects.getGameLayer().width / 2, StageObjects.getGameLayer().height / 2);
					var objRetreating:EnvObjRetreating = new EnvObjRetreating(centerStage);
					grpTickProjector.addProperty(objRetreating);
					
						//wire Collisionable to Projectable
						var collide:Collisionable = new Collisionable();
						collide.setWatchEnvObjs(StageObjects.getStageBorders());
						grpTickProjector.addProperty(collide);
						
							//reflect off walls when collode
							var reflection:ReflectCircle = new ReflectCircle(project, collide);
							grpTickProjector.addProperty(reflection);
			
			
			//Set the projection handler
			iProject = project;
				
			//Add to the property manager
			getPropertyManager().attachProperties(grpTickProjector);		
		}
		
		override public function clone():EnvObj{
			return new DirtyBubble();
		}
		
		override public function dispose(e:Event = null):void {
			//Remove this bubble
			GlobalEnvObjTable.getInstance().addEnvObj(getBubbleClassID(), this);			
			
			//destroy all the properties
			getPropertyManager().destroyProperties();
			
			//Remove this obj from the global environment table
			super.dispose(e);
		}
		
	}

}