package bb.game.components 
{	
	import bb.engine.env.components.GlobalEnvObjTable;
	import flash.display.DisplayObject;

	/**
	 * Maintains the current stage borders
	 * @author ...
	 */
	public class StageObjects
	{	
		static private var stageBorderID:String = "StageBorders";
		static private var counterBubblePlane:DisplayObject = null;
		static private var gameLayer:DisplayObject = null;
		
		static public function setStageBorders(envObjs:Array):void {
			//Can type check against the borders
			if(!GlobalEnvObjTable.getInstance().exists(stageBorderID)){
				GlobalEnvObjTable.getInstance().addEnvObjs(stageBorderID, envObjs);
			}else {
				GlobalEnvObjTable.getInstance().removeEnvObjSet(stageBorderID);
				GlobalEnvObjTable.getInstance().addEnvObjs(stageBorderID,envObjs);
			}
		}
		
		static public function getStageBorders():Array {
			return GlobalEnvObjTable.getInstance().getEnvObjs(stageBorderID);
			
		}
		
		/*
		static public function getCounterBubblePlane():DisplayObject {
			return counterBubblePlane;
		}
		
		static public function setCounterBubblePlane(dispObj:DisplayObject):void {
			counterBubblePlane = dispObj;
		}
		*/
		
		/**
		 * Returns a layer that the game mode can use.
		 */
		static public function getGameLayer():DisplayObject {
			return gameLayer;
		}
		
		static public function setGameLayer(dispObj:DisplayObject):void {
			gameLayer = dispObj;
		}
		
	}

}