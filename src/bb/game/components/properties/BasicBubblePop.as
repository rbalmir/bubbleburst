package bb.game.components.properties 
{
	import adobe.utils.CustomActions;
	import bb.engine.env.components.properties.Destroyable;
	import bb.engine.env.components.EnvObj;
	import bb.movie.IPlayMovie;
	import bb.sound.IPlaySound;
	import flash.events.Event;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class BasicBubblePop extends Destroyable 
	{
		private var movie:IPlayMovie = null;
		private var sound:IPlaySound = null;
		
		public function BasicBubblePop() 
		{
			super();
		}
		
		/**
		 * Set the movie to play when this property is invoked
		 * @param	movie
		 */
		public function setOnDestroyMovie(movie:IPlayMovie):void {
			this.movie = movie;
		}
		
		/**
		 * Set the sound to play when this property is invoked
		 * @param	movie
		 */
		public function setOnDestroySound(sound:IPlaySound):void {
			this.sound = sound;
		}
		
		override protected function propertyInvoked():void 
		{
			var handledReq:Boolean = false;
			
			//test the destruction of this object
			handledReq = testDestroy();
			
			//Implement this function 
			//handleReq = handleBasicBubblePop();
			
			//Must call back to base and specify if request was handled or not
			super.propertyInvokedAlt(handledReq);
		}
		
		private function testDestroy():Boolean {
			
			if (sound != null) {
				sound.playSound();
			}
			
			//Play specified movie or default
			if (movie != null){
				this.envObj.addChild(movie.getMovie());
				movie.playMovie();	//These movie	
			}
			
			this.envObj.destroy();
					
			return true;
		}
		
		private function handleBasicBubblePop():Boolean {
			//Think about things we want to ha[en on a Bubble Pop
			//Graphics
			//Sound
			
			return false;
		}
		
		
	}

}