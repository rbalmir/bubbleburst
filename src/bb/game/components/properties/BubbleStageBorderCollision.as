package bb.game.components.properties 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.properties.*;
	import bb.game.components.*;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.geom.Point;
	import flash.display.Stage;
	
	/**
	 * ...
	 * @author Balm Games
	 */
	public class BubbleStageBorderCollision extends Collisionable 
	{
	
		private var previousPos:Point = new Point();
		private var currentPos:Point = new Point();
		private var circlePos:Point = new Point();
		private var centerStage:Point = new Point();
		private var centerStagePat:Number = 0.0;
		private var previousPosPat:Number = 0.0;
		private var currentPosPat:Number = 0.0;
		private var closer:Boolean = true;
		
		/**
		 * Sends a notification to subscribers when the target object collides with the stage border.
		 * But on 
		 * @param	/*target
		 */
		public function BubbleStageBorderCollision() 
		{
			super(/*target*/);	
			super.setWatchEnvObjs(StageObjects.getStageBorders());
		}
		
		//This will get called on every move event to check for collision
		public function checkCollisionEvent(event:Event):void {
			
			try{
			//trace("Checking Collision Event");
			//This will fire off on every move event
			
			//Use the Parent class to get Array of borders to check hit detection from
			
			//:detection model::
			//should cache the previous position
			//to compare to the current position
			//if the previous position is farther from the center of stage
			//than current position indicates entering the stage.. ignore this case
			
			//else if the previous position is close to the center of the stage
			//than the current position, this hit detection is valid.
			
			setCenterStage();
			//setCenterStagePat();	//!This shouldnt be called
			
			setChange();
			setCirclePos();
			
			checkCircleDistance();
			hitCheck();
			
			trace("****************************") 
			trace("Center OF Stage x: " + envObj.stage.width/2 + "y: " + envObj.stage.height/2);
			trace("Previous position closer than current position: " + closer);	
			trace("Previous Length To Center: " + this.previousPosPat);
			trace("Current Length To Center: " + this.currentPosPat);
			trace("Previous Point: " + this.previousPos);
			trace("Current Point: " + this.currentPos);
			
			//:reflection model::
			//Need to do a hit test area on the bubbles edge in the quadrant towards the angle
			//where the bubble is heading to determine where the border is located.
			
			//After determining where border is located, only then can the angle be
			//adjusted to reflect properly.
			}catch (e:Error) 
			{	//this will be called if the target obj has been removed from the display list
				//trace("Fail BubbleStageBorderCollision: Most likely because target has been removed for displaylist");
			}
			
		}
		
		override public function setWatchEnvObj(envObj:EnvObj):void {
			throw new Error("BubbleStageBorderCollision:setWatchEnvObj");
	
		}
		
		override public function setWatchEnvObjs(arr:Array):void {
			throw new Error("BubbleStageBorderCollision:setWatchEnvObjs");
		}
		
		override protected function propertyInvoked():void {
			checkCollisionEvent(null);
			
		}
		
		private function getCircleP():void
		{
			circlePos.x = this.envObj.x;
			circlePos.y = this.envObj.y;
		}
		
		private function getCurrent():void
		{
			currentPos.x = circlePos.x;
			currentPos.y = circlePos.y;
		}
		
		private function getPrevious():void
		{
			previousPos.x = currentPos.x;
			previousPos.y = currentPos.y;
		}
		
		private function setChange():void
		{
			getCircleP();
			
			getPrevious();
			getCurrent();
			
		}
		
		private function setCenterStage():void
		{	
			centerStage.x = this.envObj.stage.width / 2;
			centerStage.y = this.envObj.stage.height / 2;
		}
		
		private function setCenterStagePat():void
		{
			var powX:Number = Math.pow(centerStage.x, 2);
			var powY:Number = Math.pow(centerStage.y, 2);
			
			centerStagePat = Math.sqrt((powX + powY));
		}
		
		private function setCirclePos():void
		{
			var powX:Number = Math.pow(previousPos.x, 2);
			var powY:Number = Math.pow(previousPos.y, 2);
			
			var powX1:Number = Math.pow(currentPos.x, 2);
			var powY1:Number = Math.pow(currentPos.y, 2);
			
			previousPosPat = Math.sqrt((powX + powY));
			currentPosPat = Math.sqrt((powX1 + powY1));
		}
		
		private function checkCircleDistance():void
		{
			//Take the current position, minus from the center of the stage
			//then use x diff and use this u diff for pythagorean calc
			var xdiff:Number = this.currentPos.x - this.centerStage.x;
			var ydiff:Number = this.currentPos.y - this.centerStage.y;
			currentPosPat = Math.sqrt(Math.pow(xdiff,2) + Math.pow(ydiff,2));	
			//currentPosPat = Math.sqrt(xdiff ^ 2 + ydiff ^ 2);
			//trace
			
			xdiff = this.previousPos.x - this.centerStage.x;
			ydiff = this.previousPos.y - this.centerStage.y;
			previousPosPat = Math.sqrt(Math.pow(xdiff,2) + Math.pow(ydiff,2));
			//previousPosPat = Math.sqrt(xdiff ^ 2 + ydiff ^ 2);
			
			
			if (previousPosPat < currentPosPat)
			{
				closer = true;
			}
			else 
			{
				closer = false;
			}
		}
		
		private function hitCheck():void
		{
			if (closer == false);
			{
				for (var x:int = 0; x < this.collidableEnvObjs.length; x++)
				{
					var border:EnvObj = this.collidableEnvObjs[x];
				
					if (border.hitTestObject(this.envObj) )
					{
						trace("no bubble");
					}
			}	}
		}
	}

}