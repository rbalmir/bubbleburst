/**
 * //TODO Implement handleCounterBubblesMerge
 */
package bb.game.components.properties 
{
	import bb.game.mode.LiveLong.Components.LLCounterBubble;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import bb.engine.env.components.properties.Mergeable;
	import bb.engine.env.components.properties.Collisionable;
	import bb.engine.env.components.EnvObj;
	
	/**
	 * Makes a set of counter bubbles merge
	 * @author Rich Balmir
	 */
	public class CounterBubbleMerge extends Mergeable 
	{
		//Should make a ICollisionable
		private var collisionChecker:Collisionable; 
		
		public function CounterBubbleMerge(collisionChecker:Collisionable) 
		{
			super();
			this.collisionChecker = collisionChecker;
		}

		public function setCollisionCheckHandler(collisionChecker:Collisionable):void {
			this.collisionChecker = collisionChecker;
		}
			
		/**
		 * Merge bubbles together
		 */
		override protected function propertyInvoked():void 
		{
			var handledReq:Boolean = false;
			
			//Implement this function 
			handledReq = handleCounterBubblesMerge();
			
			//Must call back to base and specify if request was handled or not
			super.propertyInvokedAlt(handledReq);
		}
		
		/**
		 * Perform the merging of counter bubbles
		 * @return Boolean If Merging was handled or not
		 */
		private function handleCounterBubblesMerge():Boolean {
			//Make this call to get array of all the counter bubbles that are overlapping 
			//This will include the target counter bubble also
			//Call: collisionChecker.getCollidingEnvObjs()
			
			//throw new Error("Not Yet Implemented");
			//For testing
			return testMerge();
			
			//Motion Code
			//We want have all the non-target bubbles center themselved on the target counter bubble
			//Keep in mind that the target and non-target bubbles will be moving.
			//In the counter bubble class I will make sure to have each IProjectable stored for manipulation
			
			//Maybe play a specific custom animation?
			//This would have to be created in the Flash CS4 IDE
			
			//We want a sound effect to play for merging
			//Note:I plan on making a Sound Factory to be able to pull sounds from a configuration
			
			return true;
		}
		
		
		private function testMerge():Boolean {
			
			var handled:Boolean = false;
			
			if (collisionChecker.getCollidingEnvObjs() != null) {
				if (collisionChecker.getCollidingEnvObjs().length > 0) {
					
				//Get the colliding objects from the Collision Handler
				//Test with making them just disappear
				for each(var envObj:EnvObj in collisionChecker.getCollidingEnvObjs()) {
					if (this.envObj != envObj){
						//remove child
						envObj.destroy();
						
						//Average the numbers
						var num1:int = (this.envObj as LLCounterBubble).getCount();
						var num2:int = (envObj as LLCounterBubble).getCount();
						
						//Set the merged bubble count
						(this.envObj as LLCounterBubble).setTime(Math.ceil(num1 + num2) / 2);
					}
						//can still hit against not visible object
						//can still hit against objects in position that have no parents
						
						//envObj.visible = false; 
						
						//Note:Typically the Bubble will have the GlobalCollisionChecker registered
						//to it in order to know when to release
						
				}
				
				if(this.envObj.scaleX < 10){
				//Scale the envObj
				this.envObj.scaleX += .4;
				this.envObj.scaleY += .4;
				}
				
				handled = true;
				
				}
			}
			
			return handled;
		}
		
		
	}

}