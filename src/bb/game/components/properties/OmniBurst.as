/*
 * //TODO Implement the handleOmniBurst
 */
package bb.game.components.properties 
{
	import bb.engine.env.components.ISupportProjectable;
	import bb.engine.env.components.properties.IProjectable;
	import bb.engine.env.components.properties.Multiplyable;
	import bb.engine.env.components.EnvObj;
	import bb.game.components.CounterBubble;
	import flash.events.Event;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class OmniBurst extends Multiplyable 
	{
		private var burstCount:int;
		private var cloneableEnvObj:EnvObj = null;
		
		public function OmniBurst(/*firstInChain:Boolean*/) 
		{
			super(/*firstInChain*/);
		}
		
		/**
		 * Sets the EnvObj that OmniBurst will burst into 
		 * @param	envObj
		 */
		public function setProjectableClonableEnvObj(envObj:EnvObj):void {
			//Should test if this supports the ISupportProjectable
			//if not throw
			if (envObj != this.envObj)
				this.cloneableEnvObj = envObj;
		}
		
		/**
		 * Sets the number of multiplied clone objects that will be generated
		 * @param	count
		 */
		public function setBurstCount(count:uint):void {
			this.burstCount = count;
		}
		
		/**
		 * Invoke OmniBurst of cloned EnvObjs
		 */
		override protected function propertyInvoked():void 
		{
			var handledReq:Boolean = false;
			
			//For testing: Only burst if count greater than zero
			if(burstCount > 0)
				handledReq = testOmniBurst();
			
				handledReq = true;
				
			//Implement this function 
			//handleOmniBurst();
			
			//Must call back to base and specify if request was handled or not
			super.propertyInvokedAlt(handledReq);
		}
		
		//makes an OmniBurst
		private function testOmniBurst():Boolean {
		
			//get angle partition
			var angleDelta:Number = getAnglePartition();
			
			var iProject:IProjectable;
					
			//for each count create clone to burst outward
			for (var i:int = 0; i < this.burstCount; i++) {
				
				//Get the Projectable
				iProject = initCloneGetIProjectable();
				
				//Set the omni projection
				if(iProject != null){
					iProject.setProjection( 
						Math.floor(angleDelta * i) 
						,iProject.getProjectionMag());
				}
			}
			
			return true;
		}
		
		//Make a clone, add to targets parent and return the Projection handler for the clone
		private function initCloneGetIProjectable():IProjectable {
			
			var clone:EnvObj = null;
			var clonesProjectable:IProjectable = null;
			
			if(this.envObj is ISupportProjectable){
				
				clone = getAClone();
				clone.cacheAsBitmap = true;
				
				if(clone is ISupportProjectable){
					
					//set clones position
					clone.x = envObj.x;
					clone.y = envObj.y;
					
					//add clone to the targets parent obj
					this.envObj.parent.addChild(clone);
					
					//Get the IProjectable from the clone
					clonesProjectable = (clone as ISupportProjectable).getIProjectable();
					
					//Set the magnitude for the clones projection from targets speed
					clonesProjectable.setProjection(0,
						(this.envObj as ISupportProjectable).getIProjectable().getProjectionMag());
				
				}
				
			}
			
			return clonesProjectable;
		}
		
		//generates a clone of the current EnvObj target object if target object not set
		private function getAClone():EnvObj {
			var clone:EnvObj = null;
			
				if (cloneableEnvObj == null){
					clone = this.envObj.clone();
				}else
					clone = cloneableEnvObj.clone();
					
			return clone;
		}
		
		//get angle partition
		private function getAnglePartition():Number {
			return (360 / burstCount);
		}
		
	}

}