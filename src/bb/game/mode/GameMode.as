package bb.game.mode 
{
	import bb.game.AppConfig;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author ...
	 */
	
	public class GameMode 
	{
		static private var appLayers:Dictionary = null;
		
		public function GameMode() 
		{
			if (appLayers == null){
				appLayers = new Dictionary();
				initAppLayers();
			}
		}
		
		private function initAppLayers():void {
			appLayers[AppLayersID.MAINMENULAYER] = new Sprite();
			appLayers[AppLayersID.MODEMENULAYER] = new Sprite();
			appLayers[AppLayersID.GAMELAYER] = new Sprite();
			
			AppConfig.getRootLayer().addChild(appLayers[AppLayersID.GAMELAYER]);			
			AppConfig.getRootLayer().addChild(appLayers[AppLayersID.MODEMENULAYER]);
			AppConfig.getRootLayer().addChild(appLayers[AppLayersID.MAINMENULAYER]);
		}
		
		protected function getGetGameLayer():Sprite {
			return appLayers[AppLayersID.GAMELAYER];
		}
		
		protected function getModeMenuLayer():Sprite {
			return appLayers[AppLayersID.MODEMENULAYER];
		}
		
		protected function resetGameLayer():void {
			AppConfig.getRootLayer().removeChild(appLayers[AppLayersID.GAMELAYER]);
			appLayers[AppLayersID.GAMELAYER] = new Sprite();
			AppConfig.getRootLayer().addChild(appLayers[AppLayersID.GAMELAYER]);
		}
		
	}

}