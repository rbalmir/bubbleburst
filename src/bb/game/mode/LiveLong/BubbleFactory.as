package bb.game.mode.LiveLong 
{
	import bb.engine.env.components.PropertyManager;
	import bb.game.components.Bubble;
	import bb.game.components.RegularBubble;
	import bb.game.mode.LiveLong.Components.LLCounterBubble;
	import bb.game.mode.LiveLong.Components.LLDirtyBubble;
	import bb.game.mode.LiveLong.Components.LLRegularBubble;
	/**
	 * TODO: Change the Bubble types to enumerations or strings. Change the bubble selection logic.
	 * 
	 * Creates bubbles also, knows when bubbles are destroyed as it registers
	 * a destroy method
	 * ...
	 * @author rich
	 */
	public class BubbleFactory 
	{
		var controlMethods:ControlMethods = null;
		public function BubbleFactory(controlMethods:ControlMethods) 
		{
			this.controlMethods = controlMethods;
		}
		
		public function getRandomBubble(currentLevel:int):Bubble {
			
			var typeOfBubble:int = controlMethods.calcSelectRandomBubble();
			var bub:Bubble = null;
			
			switch(typeOfBubble) {
				case 1:	//regular
				bub = new LLRegularBubble();
				break;
				
				case 2:	//counter
				bub = new LLCounterBubble(1);
				break;
				
				case 3:	//dirty
				bub = new LLDirtyBubble();
				break;
				
			default:
				break;
			}
			
			return bub;
		}
		
	}

}