package bb.game.mode.LiveLong 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.ISupportProjectable;
	import bb.engine.env.components.properties.IProjectable;
	import bb.engine.env.components.properties.Projectable;
	import bb.game.components.Bubble;
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author rich
	 */
	public class BubbleInjector 
	{
		//quadrant
		var q1:Point, q2:Point, q3:Point , q4:Point;
		var borderOutSet:Number = 0;
		var borderOnSet:Number = 0;
		var squarePlane:Sprite;
		
		/**
		 * plane to set positions against
		 * borderOnset square points to use boundaries for injection max and min angles
		 * borderOutset how far out from the border will the object be injected from
		 * @param	squarePlane
		 * @param	borderOnset
		 * @param	borderOutSet
		 */
		public function BubbleInjector(squarePlane:Sprite, borderOnSet:Number, borderOutSet:Number) 
		{
			this.squarePlane = squarePlane;
			
			var swidth:Number, sheight:Number;
			//q1
			swidth = squarePlane.stage.stageWidth;
			sheight = squarePlane.stage.stageHeight;
			
			q1 = new Point(swidth - borderOnSet, 0 + borderOnSet);
			q2 = new Point(borderOnSet, borderOnSet);
			q3 = new Point(borderOnSet, sheight - borderOnSet);
			q4 = new Point(swidth - borderOnSet, sheight - borderOnSet);
			
			this.borderOutSet = borderOutSet;
			this.borderOnSet = borderOnSet;
		}
		
		public function randomSideInject(bubble:ISupportProjectable):void {
			topInject(bubble);
		}
		
		/**
		 * Places the bubble at random position on the stage
		 * Note:Does not addChild to parent
		 * @param	bubble
		 */
		public function randomStagePlacement(bubble:EnvObj):void {
			/*//TODO:Should take in account the width of the circle child
			 */
			var x:int;
			var y:int;
			var margin:int = bubble.width;
			
			x = ((this.squarePlane.stage.stageWidth - margin) * Math.random()) + (margin/2);
			y = ((this.squarePlane.stage.stageHeight - margin) * Math.random()) + (margin/2);
			
			//bubble.x = x;
			//bubble.y = y;
			(bubble as Bubble).getIProjectable().setPosition(new Point(x, y));
			(bubble as Bubble).getIProjectable().setProjection(1,1);
			//(bubble as IProjectable).setPosition(new Point(x, y));
		}
		
		private function topInject(bubble:ISupportProjectable):void {
			//get random number for inject position
			
			var x:Number = q2.x + ((q1.x - q2.x) * Math.random()); 
			var y:Number = 0 - borderOutSet;
			var startPoint:Point = new Point(x,y);
			
			//get ran
			//use the start point to determine vector to 
			//var dely:Number = q2.y - startPoint.y;
			//var delx:Number = startPoint.x - q2.x;
			//var minAngle:Number = Math.atan2((dely) , delx);
			var dely:Number = q2.y - startPoint.y;
			var delx:Number = q2.x - startPoint.x; 
			var minAngle:Number = Math.atan2((dely * -1) , delx);
			
			//var adely:Number = q1.y - startPoint.y;
			//var adelx:Number = startPoint.x - q1.y;
			//var maxAngle:Number = Math.atan2(adely * -1, adelx);
			var adely:Number = q1.y - startPoint.y;
			var adelx:Number = q1.x - startPoint.x;
			var maxAngle:Number = Math.atan2(adely * -1, adelx);
			
			var delta:Number = minAngle - maxAngle;
			var injectAngleRad:Number = minAngle + ((Math.abs(delta)) * Math.random());
			var injectAngleDeg:Number =  Projectable.radToDeg(injectAngleRad);
			
			trace("[Injection Angle deg:]" + injectAngleDeg);
			
			bubble.getIProjectable().setPosition(startPoint);
			bubble.getIProjectable().setProjection(injectAngleDeg, bubble.getIProjectable().getProjectionMag());
		}
		
	}

}