package bb.game.mode.LiveLong 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import bb.game.components.properties.*;
	import bb.game.components.Bubble;
	import bb.sound.BasicSound;
	import flash.geom.Point;
	import bb.game.mode.LiveLong.Components.*;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author rich
	 */
	public class BubbleProperties 
	{
		private static var funcBubDestroyed:Function = null;
		private static var funcBubMerged:Function = null;
		private static var gamePlane:Sprite = null;
		private static var stageBorders:Array = null;
		private static var sndBubblePop:BasicSound = null;
		public var accessibleProps:Dictionary = null;
		
		public function BubbleProperties() 
		{					
			if (BubbleProperties.gamePlane == null || BubbleProperties.stageBorders == null)
				throw new Error("BubbleProperties:Ctor static gamePlane | stageBorders not set. Use BubbleProperties.setGamePlane()");
				
			accessibleProps = new Dictionary();
		}
		
		//Plane in which is build on
		static public function setGamePlane(gamePlane:Sprite):void {
			BubbleProperties.gamePlane = gamePlane;
		}
		
		//Borders which all objects will run into
		static public function setStageBorders(stageBorders:Array):void {
			BubbleProperties.stageBorders = stageBorders;
		}
		
		static public function setSoundBubblePop(basicSound:BasicSound):void {
			sndBubblePop = basicSound;
		}
		
		static public function getSoundBubblePop():BasicSound {
			if(sndBubblePop != null)
				return sndBubblePop;
			
			return null;
		}
		
		public function counterBubbleProperties(targetEnvObj:LLCounterBubble):PropertyManager {
		//intend to let the DestroyBubble and OmniBurst be accessible
		clearAccesibleProps();	
			
		var propMgr:PropertyManager = new PropertyManager(targetEnvObj);
			
		//1)Handles mouse click, 
		//2)merging of counter bubbles, 
		//3)omniburst into regular bubbles
		//4)and destruction of target bubble.
		//---------------------------------------
		var grpTouch2Transform:GroupedProperties = new GroupedProperties("Touch->MergableooMultiplyable");
					
		//Create Touchable
		var aClick:Touchable = new MouseClicked();
		grpTouch2Transform.addProperty(aClick);
		
		//Collisionable Checker
		var aCollisionable:Collisionable = new GlobalCollisionCheck(targetEnvObj.getBubbleClassID());
		grpTouch2Transform.addProperty(aCollisionable, false);
		
		//wire Mergable to Touchable 
		var aMerge:Mergeable = new CounterBubbleMerge(aCollisionable);
		grpTouch2Transform.addProperty(aMerge);
		aMerge.addEventListener(aMerge.getPropertyID(), this.notifyBubbleMerge);
		
		//chain Multiplyable to Mergable
		var omniBurst:OmniBurst = new OmniBurst();
		targetEnvObj.setOmniBurst(omniBurst);
		omniBurst.setProjectableClonableEnvObj(new LLRegularBubble()); //burst into regular bubbles
		aMerge.setNextInChain(omniBurst);
			//allow the omniBurst to be accessible
			accessibleProps["OMNIBURST"] = omniBurst; 
			
		//Add to the property manager
		//getPropertyManager().attachProperties(grpTouch2Transform);
		propMgr.attachProperties(grpTouch2Transform);
		
		//add burst as unwired since chained to mergable
		//grpTouch2Transform.addProperty(omniBurst, false);
		
		var grpBurst2Destroy:GroupedProperties = new GroupedProperties("Multiplyable->Destroyable");
		grpBurst2Destroy.addProperty(omniBurst);

		//wire Destroyable to Multiplyable
		var aDestoryBub:BasicBubblePop = new BasicBubblePop();
		aDestoryBub.setOnDestroySound(BubbleProperties.getSoundBubblePop());
		grpBurst2Destroy.addProperty(aDestoryBub);
			//Allow the destruction be accessible
			accessibleProps["BASICBUBBLEPOP"] = aDestoryBub; 
			
		//Add to the property manager
		//getPropertyManager().attachProperties(grpBurst2Destroy);
		propMgr.attachProperties(grpBurst2Destroy);
		
		//!!Note should remove like wise
		//This bubble wants to know when its destroyed
		//aDestoryBub.addEventListener(aDestoryBub.getPropertyID(), dispose);
		//CONCERN:These end off may not properly be garbage collected
		aDestoryBub.addEventListener(aDestoryBub.getPropertyID(),this.notifyBubbleDestroyed);
		
		//aDestoryBub.addEventListener(aDestoryBub.getPropertyID(),(aDestoryBub.getEnvObj() as Bubble).dispose);
		
		//handle bounce
		var grpTickProjector:GroupedProperties = makeWallBounceGP(targetEnvObj);
		
		//Add to the property manager
		//getPropertyManager().attachProperties(grpTickProjector);
		propMgr.attachProperties(grpTickProjector);
		
		return propMgr;
	}
	
		public function regularBubbleProperties(targetEnvObj:LLRegularBubble):PropertyManager {
			
			var propMgr:PropertyManager = new PropertyManager(targetEnvObj);
		
			//1)Handles mouse click, 
			//2)and destruction of target bubble.
			//---------------------------------------
			var grpTouch2Destroy:GroupedProperties = new GroupedProperties("Touchable->Destroyable");
			
			//Create Touchable:
			var aClick:Touchable = new MouseClicked();
			grpTouch2Destroy.addProperty(aClick);
			
			//wire Destroyable to Touchable
			var aDestoryBub:BasicBubblePop = new BasicBubblePop();
			aDestoryBub.setOnDestroySound(BubbleProperties.getSoundBubblePop());
			aDestoryBub.setEnvObj(targetEnvObj);
			
			grpTouch2Destroy.addProperty(aDestoryBub);
			
			//Add to the property manager
			propMgr.attachProperties(grpTouch2Destroy);
						
			//Listen for when bubble is destroyed
			//aDestoryBub.addEventListener(aDestoryBub.getPropertyID(), dispose);
			aDestoryBub.addEventListener(aDestoryBub.getPropertyID(),this.notifyBubbleDestroyed);		
			//aDestoryBub.addEventListener(aDestoryBub.getPropertyID(),(aDestoryBub.getEnvObj() as Bubble).dispose);
			
			//Handle bouncing off walls
			var grpTickProjector:GroupedProperties = makeWallBounceGP(targetEnvObj);

			//Add to the property manager
			propMgr.attachProperties(grpTickProjector);	
			
			return propMgr;
		}
		
		public function dirtyProperties(targetEnvObj:LLDirtyBubble):PropertyManager {
						
			var propMgr:PropertyManager = new PropertyManager(targetEnvObj);
			
			//Handles bouncing of the 
			var grpTickProjector:GroupedProperties = makeWallBounceGP(targetEnvObj);

			//Add to the property manager
			propMgr.attachProperties(grpTickProjector);	
			
			return propMgr;
		}

		public static function regBubbleDestroyed(func:Function):void {
			BubbleProperties.funcBubDestroyed = func;	
		}
		
		public static function counterBubbleMerged(func:Function):void {
			BubbleProperties.funcBubMerged = func;	
		}
		
		//
		private function notifyBubbleDestroyed(e:Event = null):void {
			if(BubbleProperties.funcBubDestroyed != null)
				BubbleProperties.funcBubDestroyed.call(null, e);
			
			//destroy the target obj
			var prop:EnvObjProperty = e.target as EnvObjProperty; 
			(prop.getEnvObj() as Bubble).dispose();
			
			//note:this does not effect those propeties
			clearAccesibleProps();
		}
		
		private function clearAccesibleProps():void {
			for (var id:String in accessibleProps) {
				delete accessibleProps[id];
			}
		}
		
		private function notifyBubbleMerge(e:Event = null):void {
			if(BubbleProperties.funcBubMerged != null)
				BubbleProperties.funcBubMerged.call(null,e);
		}
		
		//Make the Wall Bounce Grouped Properties
		private function makeWallBounceGP(targetEnvObj:EnvObj):GroupedProperties
		{
			//1)On Enter Frame Clock
			//2)Project across stage linearly
			//3)When wall detected, reflect accordingly
			//----------------------------------------
			var grpTickProjector:GroupedProperties = new GroupedProperties("Clockable->Projectable->Collisionable");
			
			//Create Clockable
			var tick:Clockable = new EnterFrameClock();
			grpTickProjector.addProperty(tick);
			
			//wire Projectable to clockable
			var project:Projectable = new LinearMovement();
			grpTickProjector.addProperty(project);
					
					/*
					//Determines if object is retreating from center of stage
					var centerStage:Point = new Point(BubbleProperties.gamePlane.width / 2, BubbleProperties.gamePlane.height / 2);
					var objRetreating:EnvObjRetreating = new EnvObjRetreating(centerStage);
					grpTickProjector.addProperty(objRetreating);
					*/
					
						//wire Collisionable to Projectable
						var collide:Collisionable = new Collisionable();
						collide.setWatchEnvObjs(BubbleProperties.stageBorders);
						grpTickProjector.addProperty(collide);
						
							//reflect off walls when collode
							var reflection:ReflectCircle = new ReflectCircle(project, collide);
							grpTickProjector.addProperty(reflection);
			
			//Set the projection handler
			(targetEnvObj as Bubble).setIProjectable(project);
		
			return grpTickProjector;
		}
	}

}