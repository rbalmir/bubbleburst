package bb.game.mode.LiveLong.Components 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import bb.game.components.Bubble;
	import bb.game.components.properties.BasicBubblePop;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.game.components.properties.CounterBubbleMerge;
	import bb.game.components.properties.OmniBurst;
	import bb.game.mode.LiveLong.BubbleFactory;
	import bb.engine.env.components.GlobalEnvObjTable;
	import bb.game.mode.LiveLong.BubbleProperties;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.sampler.NewObjectSample;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	/**
	 * Counter Bubble counts upward.  When the bubble is hit.  It will burst into
	 * the current counter value.  When hit on top of the another counter, those 
	 * bubbles will merge and average thier values
	 * @author Rich Balmir
	 */
	public class LLCounterBubble extends Bubble implements ISupportProjectable
	{
		static public const ID:String = "CounterBubble";
		
		//private var iProject:IProjectable = null;
		private var tfCounter:TextField= null;
		
		private var bubbleBaseColor:uint = 0x333333;
		private var counterCircleColor:uint = 0xFFDDFF;
		
		private var colorBlue:uint = 0x0000dd;
		private var colorRed:uint = 0xFF0000;
		
		private var omniBurst:OmniBurst;
		static private var timer:Timer = null;
		
		private var aDestroyBub:BasicBubblePop;
		
		private var counter:int;
		private var counterDelay:int = 2000;
		
		private var shouldICountUp:Boolean;
		
		public function LLCounterBubble(scale:Number = 1) 
		{
			super(ID);
			initialize(scale);
		}
		
		/**
		 * Draw bubble.. set timer counter.. config env properties
		 */
		protected function initialize(bubScale:Number) {

			//make the counter bubble
			drawBubble(bubScale);
			
			//register to a second interval clockable
			//to handle the counter changing
			if(LLCounterBubble.timer == null)
				LLCounterBubble.timer = new Timer(counterDelay, 10000);
			
			//create the timer
			timer.addEventListener(TimerEvent.TIMER, this.countEvent);
			timer.start();
			
			//configure
			configProperties();
			
			//activate the bubbles properties
			this.getPropertyManager().activateProperties();
			
			//Register this bubble to the global environmental object table
			GlobalEnvObjTable.getInstance().addEnvObj(ID, this);
		}
		
		/**
		 * Get the IProjectable for this object
		 * @return
		 */
		/*
		public function getIProjectable():IProjectable {
			return iProject;
		}
		*/
		
		/**
		 * Get the counters time
		 * @return
		 */
		public function getCount():int {
			//Get time from the count down Property
			return counter;
		}
		
		public function countUp(shouldICountUp:Boolean):void {
			this.shouldICountUp = shouldICountUp;
		}
		
		/**
		 * Set the counters time
		 * @param	modCount
		 */
		public function setTime(modCount:int):void {
			counter = modCount;
			this.tfCounter.text = modCount.toString();
			
			if(this.counter < 0)
				this.tfCounter.textColor = colorRed;
			else
				this.tfCounter.textColor = colorBlue;
			
		}
		
		/**
		 * Sets
		 * @param	sec
		 */
		public function setCounterDelay(sec:int) {
			counterDelay = sec;
		}
		
		public function setOmniBurst(oBurst:OmniBurst):void {
			this.omniBurst = oBurst;
		}
		
		public function getOmniBurst():OmniBurst {
			return this.omniBurst;
		}
		/*
		public function setIProjectable(iP:IProjectable):void {
			this.iProject = iP;
		}
		*/
		private function drawBubble(scale:Number):void {
		
			//create the base of the bubble and add it to this object
			var bubbleBase:Sprite = addTestBubble(0x333333);
			var innerWidth:Number = (bubbleBase.width * .70);
			
			//make the counter circle and add to base circle
			var counterCircle:Sprite = makeTestBubble(counterCircleColor, innerWidth / 2);
			bubbleBase.addChild(counterCircle);
			
			//create text, bound text boundaries to counter circle limits
			tfCounter = new TextField();
			tfCounter.text = "";
			tfCounter.width = counterCircle.width;
			tfCounter.height = counterCircle.height;
			setTFCounterProperties(tfCounter);
			
			//shift text
			tfCounter.x = tfCounter.x - tfCounter.width / 2;
			tfCounter.y = tfCounter.y - tfCounter.width / 2;
			
			counterCircle.scaleX = scale;
			counterCircle.scaleY = scale;
			
			//Add counter text
			counterCircle.addChild(tfCounter);
						
			trace("couterCircleWidth" + counterCircle.width);
			trace("width:" + tfCounter.width + "tfCounter.x:" + tfCounter.x);
			trace("twidth:" + tfCounter.textWidth + "tfCounter.y:" + tfCounter.y);
		}
		
		private function setTFCounterProperties(tfCounter:TextField):void {
			
			var tf:TextFormat = new TextFormat(null, 21, null, null, null, null, null, null, TextFormatAlign.CENTER, null, null, null, null);
			//tfCounter.setTextFormat(tf);
			tfCounter.defaultTextFormat = tf;
			tfCounter.textColor = colorBlue;
			tfCounter.selectable = false;
			//tfCounter.backgroundColor = 0xAAAAAA;
			//tfCounter.background = true;
			
			/*
			var css:StyleSheet =  new StyleSheet();
			css.
			tfCounter.styleSheet = 
			*/
		}
		
		//Configure the properties that make a CounterBubble
		protected function configProperties():void {
			
			//Call Bubble Factory to get a property manager
			var bP:BubbleProperties = new BubbleProperties();
			var pM:PropertyManager = bP.counterBubbleProperties(this);
			pM.setTargetEnvObj(this);
			
			this.setPropertyManager(pM);
			
			//Need away to get a handle back onto omniburst and bubble destroyed
			//have the bubble properties store them in a exepected handles property hash
			//with specific know ids
			
			if("OMNIBURST" in bP.accessibleProps)
				this.omniBurst = bP.accessibleProps["OMNIBURST"];
			else
				throw new Error("LLCounterBubble:config The bubbleProperties not accessible");
			
			if("BASICBUBBLEPOP" in bP.accessibleProps)
				this.aDestroyBub = bP.accessibleProps["BASICBUBBLEPOP"];
			else 
				throw new Error("LLCounterBubble:config The bubbleProperties not accessible");
	
		}
				
		private function countEvent(e:Event):void {
			//Increase the counter
			if(this.shouldICountUp == true)			
				this.counter++;
			else
				this.counter--;
				
				//update the omniburst property
				this.omniBurst.setBurstCount(this.counter);
				
				if(this.counter < 0){
					this.tfCounter.textColor = colorRed;
				}else {
					this.tfCounter.textColor = colorBlue;
				}
					
				//force burst
				if(Math.abs(this.counter) > 99)
					this.aDestroyBub.invokeProperty();
				
				//Update the Text on this bubble
				this.tfCounter.text = Math.abs(this.counter).toString();
		}
		
		override public function dispose(e:Event = null):void 
		{
			//Remove this bubble
			GlobalEnvObjTable.getInstance().removeEnvObj(getBubbleClassID(), this);			
			
			//destroy all the properties																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																							
			getPropertyManager().destroyProperties();
			
			if(timer != null)
				timer.removeEventListener(TimerEvent.TIMER, this.countEvent);
			
			this.omniBurst = null;
			
			//Remove this obj from the global environment table
			super.dispose(e);
			
			//
			//Counter bubble should be destroyed
		}
		
		override public function destroy(preservePM:Boolean = false):void {
			
			timer.removeEventListener(TimerEvent.TIMER, this.countEvent);
			super.destroy();
		}
		
		override public function clone():EnvObj{
			return new LLCounterBubble();
		}
		
	}

}