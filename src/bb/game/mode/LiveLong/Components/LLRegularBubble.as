package bb.game.mode.LiveLong.Components 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import bb.game.components.Bubble;
	import bb.game.mode.LiveLong.BubbleProperties;
	import flash.display.Sprite;
	import bb.game.components.properties.*;
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class LLRegularBubble extends Bubble implements ISupportProjectable 
	{
		static public const ID:String = "RegularBubble";
		//private var iProject:IProjectable = null;
		
		public function LLRegularBubble() 
		{
			super(ID);
			
			initialize();
		}
		
		/* INTERFACE bb.engine.env.components.ISupportProjectable */
		/*
		public function getIProjectable():IProjectable 
		{
			return iProject;
		}
		
		public function setIProjectable(iP:IProjectable):void {
			this.iProject = iP;
		}
		*/
		private function initialize():void {
			
			//make the bubble graphic
			drawBubble();
			
			//configure
			configProperties();
			
			//activate the bubbles properties
			this.getPropertyManager().activateProperties();
			
			//Add the to global environment
			GlobalEnvObjTable.getInstance().addEnvObj(getBubbleClassID(), this);		
		}
		
		private function drawBubble() {
		
			//create the base of the bubble and add it to this object
			var bubbleBase:Sprite = addTestBubble(0x33FFFF);
		}
		
		//Configure the properties that make a CounterBubble
		protected function configProperties():void {

			//Call Bubble Factory to get a property manager
			var bP:BubbleProperties = new BubbleProperties();
			var pM:PropertyManager = bP.regularBubbleProperties(this);
			pM.setTargetEnvObj(this);
			
			this.setPropertyManager(pM);
			
		}
		
		override public function clone():EnvObj{
			return new LLRegularBubble();
		}
		
		override public function dispose(e:Event = null):void {
			//Remove this bubble
			//GlobalEnvObjTable.getInstance().addEnvObj(getBubbleClassID(), this);			
			GlobalEnvObjTable.getInstance().removeEnvObj(getBubbleClassID(), this);			
			
			//destroy all the properties
			getPropertyManager().destroyProperties();
			
			//Remove this obj from the global environment table
			super.dispose(e);
			
			
		}
				
	}

}