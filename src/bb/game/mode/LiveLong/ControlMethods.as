package bb.game.mode.LiveLong 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author rich
	 */
	public class ControlMethods 
	{
		private var bubbleInjectionIntervalCurve:Array;
		private var numOfBubbleToStartLevelCurve:Array;
		private var bubbleSpeedCurve:Array;
		private var maxBubbleSpeed:int = 50;
		
		public function ControlMethods() 
		{
			//build Bubble Interval
			this.bubbleInjectionIntervalCurve = buildBubbleInjectionIntervalCurve();
			
			//build num of bubble curve
			this.numOfBubbleToStartLevelCurve = buildNumBubblesToStartLevel();
			
			//build bubble speed curve
			this.bubbleSpeedCurve = buildBubbleSpeedCurve();
		}	
			
		public function counterDirectionUp():Boolean {
			
			var r:Number = Math.random();
			
			if (r > .5)
				return true;
			else
				return false;		
		}
		
			/**
			 * Asymtoptically early decreasing curve
			 * plot ((10 - x)^2) between 0 and 10
			 * @param seconds into game
			 * @return milliseconds
			 */
			public function calcBubbleInjectionInterval(currentLevel:int):Number {
				var value:Number; 
				
				try
				{
					value = this.bubbleInjectionIntervalCurve[currentLevel]
				}catch (e:Error) {
					var len:int = this.bubbleInjectionIntervalCurve.length;
					value = this.bubbleInjectionIntervalCurve[len - 1];
				}
				
				return value;
			}

			public function calcNumBubblesToStartLevel(level:int):int {
				var value:int; 
				
				try
				{
					value = Math.ceil(this.numOfBubbleToStartLevelCurve[level]);
				}catch (e:Error) {
					var len:int = this.numOfBubbleToStartLevelCurve.length;
					value = Math.ceil(this.numOfBubbleToStartLevelCurve[len - 1]);
				}
				
				return value;
			}

			public function calcBubbleSpeed(level:int):Number {
				var value:Number; 
				
				try
				{
					value = Math.random() * this.bubbleSpeedCurve[level];
				}catch (e:Error) {
					var len:int = this.bubbleSpeedCurve.length;
					value = this.bubbleSpeedCurve[len - 1];
				}
				
				return value;
			}
			
			/**
			 * Returns a number between 1 and 3
			 * TODO: Implement the Bubble Selection algorithm
			 * @return
			 */
			public function calcSelectRandomBubble():int {
				return Math.ceil(Math.random() * 3);
			}
			
						/**
			 * Produces a random selection of enum that determines the number of bubbles
			 * that should be injected into the screen
			 * At level one all categories uniformly distributed and levels increase distribution
			 * favors larger number of injection
			 * As time
			 * Small 1-3 Mid 3-7 Large 7-10 
			 * @return number of bubbles to inject
			 * @param	timeTicks
			 */
			public function calcNumBubblesToInject(timeTicksInCurrentLevel:int, CurrentLevel:int):int {
			
				//Make large share weighted as levels increase
				var sShare:Number = 1 + (1/(CurrentLevel * 2 + 1/Math.pow(timeTicksInCurrentLevel,2)));
				var mShare:Number = 1 + (1/(CurrentLevel * 1.5 + 1/Math.pow(timeTicksInCurrentLevel,1.5)));
				var lShare:Number = 1 + (1/(CurrentLevel * 1 + 1/Math.pow(timeTicksInCurrentLevel,1)));
				
				var total:Number = sShare + mShare + lShare;
				
				var sPercent:Number, mPercent:Number, lPercent:Number;
				sPercent = sShare / total;
				mPercent = mShare / total;
				lPercent = lShare / total;
				
				var magicNum:Number = Math.random();
				//Partion Points
				//[1...2....3.....4]
				var numToInject:Number;
				if (magicNum < sPercent) 
				{
					//small
					numToInject = 0 + Math.ceil(Math.random() * 3);				
				}else if ((sPercent < magicNum )&& (magicNum < (sPercent + mPercent))) 
				{
					//medium
					numToInject = 3 + Math.ceil(Math.random() * 4);
				}else if (((sPercent + mPercent) < magicNum) && (magicNum < (sPercent + mPercent + lPercent))) 
				{
					numToInject = 7 + Math.ceil(Math.random() * 5);
				}
				
			return numToInject;			
			}
			
			/**
			 * Only everylevel increase the total numebr of bubbles required to burst
			 * will increase by 20
			 * @param	level
			 * @return
			 */
			public function calcNumBubblesToPopForLevelComplete(level:int):int {
				var x:int, y:int;
				x = level;
				
				y = 2 * (x - 1) + 1;
				return y; 
			}
			
			/**
			 * Used to get a random number to use for the counter bubble
			 * initial count;
			 * @param	level
			 * @return
			 */
			public function calcCounterBubbleCount(level:int):int 
			{
				var x:int, y:int;
				x = level;
				
				y = 2 * x;
				return Math.ceil(Math.random() * y);
			}
			
			/**
			 * used to get a random number to scale the size of the
			 * counter bubbles.  Max is 4
			 * @param	level
			 * @return
			 */
			public function calcCounterSize(level:int):Number 
			{
				var x:int, y:int;
				x = level;
				
				y = (-3/100 * x) + 4;
				return Math.ceil(Math.random() * y);
			}
			
			/**
			 * used to get a random number to scale the size of the
			 * counter bubbles.  Max is 4
			 * @param	level
			 * @return
			 */
			public function calcDirtySize(level:int):Number 
			{
				var x:Number, y:Number;
				x = level;
				
				y = (4/25 * x) + 2;
				return (y);
			}
			
			//puts together the curve in which controls bubble injection interval
			private function buildBubbleInjectionIntervalCurve():Array{
			
				//supports normalizing against x range
				var partitions:Number = 100; //range to consider to normalize to				
				var maxInjSec:Number = 3000;
				
				var upperBound:int = 10;  
				var interval:Number = upperBound/partitions; 

				//Build curve
				var point:Number;
				var curvePoints:Array = new Array();				
				for (var i:Number = 0; i < upperBound; i=i+interval) {
					point = Math.pow(upperBound - i,2)
					curvePoints.push(point);
				}
				
				//support normalizing
				var maxP:Number = getMax(curvePoints);
				var minP:Number = getMin(curvePoints);
				
				//Normalize
				var normalized:Array = normalizeY(curvePoints, maxP, minP);
				var xFormed:Array = new Array();
				
				//transform to desired Yo range
				for (var j:int = 0; j < normalized.length; j++) {
					xFormed.push((maxInjSec * normalized[j]) + 300);
				}
				
				return xFormed;
			}

			//puts together the curve in which controls bubble number to start level
			private function buildNumBubblesToStartLevel():Array{
			
				//supports normalizing against x range
				var numberOfLevels = 100; //range to consider to normalize to				
				//var maxInjSec = 4000;
				
				var upperBound:int = 10;  
				var interval:Number = upperBound/numberOfLevels; 

				//Build this curve
				var point:Number;
				var curvePoints:Array = new Array();				
				for (var i:Number = 0; i < upperBound; i=i+interval) {
					point = -1 * Math.pow(upperBound - i,2)
					curvePoints.push(point);
				}
				
				//support normalizing
				var maxP:Number = getMax(curvePoints);
				var minP:Number = getMin(curvePoints);
				
				//Normalize
				var normalized:Array = normalizeY(curvePoints, maxP, minP);
				var xFormed:Array = new Array();
				
				//transform to desired Yo range
				var maxNumBubblesToStart = 30;
				for (var j:int = 0; j < normalized.length; j++) {
					xFormed.push((maxNumBubblesToStart * normalized[j]) + 1);
				}
				
				return xFormed;
			}

			//bubble speed
			private function buildBubbleSpeedCurve():Array{
			
				//supports normalizing against x range
				var numberOfLevels = 100; //range to consider to normalize to				
				//var maxInjSec = 4000;
				
				var upperBound:int = 10;  
				var interval:Number = upperBound/numberOfLevels; 

				//Build this curve
				var point:Number;
				var curvePoints:Array = new Array();				
				for (var i:Number = 0; i < upperBound; i=i+interval) {
					point = -1 * Math.pow(upperBound - i,2)
					curvePoints.push(point);
				}
				
				//support normalizing
				var maxP:Number = getMax(curvePoints);
				var minP:Number = getMin(curvePoints);
				
				//Normalize
				var normalized:Array = normalizeY(curvePoints, maxP, minP);
				var xFormed:Array = new Array();
				
				//transform to desired Yo range
				//var maxNumBubblesToStart = 30;
				for (var j:int = 0; j < normalized.length; j++) {
					xFormed.push((this.maxBubbleSpeed * normalized[j]) + 1);
				}
				
				return xFormed;
			}

			//Normalizes set of plot in a desired ranges
			private function normalizeY(curvePoints:Array, maxVal:Number,  minVal:Number):Array {
				var normArr:Array = new Array();
				
				//Normalize
				for (var i:int = 0; i < curvePoints.length; i++) {
					normArr.push((curvePoints[i] - minVal) / (maxVal - minVal));
				}
				
				return normArr;
			}
			
			//get max value out of array
			private function getMax(curvePoints:Array):Number {
				var maxP:Number=Number.MIN_VALUE;	   	//should be the mini of what normailizing against
				var point:Number;
				
				for (var i:int = 0; i < curvePoints.length; i++) {
					point = curvePoints[i];
					
					if (point > maxP)
						maxP = point;
				}
				return maxP;
			}
			
			//get min value from array
			private function getMin(curvePoints:Array):Number {
				var minP:Number=Number.MAX_VALUE;	   	//should be the mini of what normailizing against
				var point:Number;
				
				for (var i:int = 0; i < curvePoints.length; i++) {
					point = curvePoints[i];
					
					if (point < minP)
						minP = point;
				}
				
				return minP;
			}
		
	}
		
		
	

}