package bb.game.mode.LiveLong 
{
	import bb.engine.env.components.properties.EnvObjProperty;
	import bb.engine.env.components.properties.IProjectable;
	import bb.game.components.Bubble;
	import bb.game.components.DirtyBubble;
	import bb.game.mode.LiveLong.Components.LLCounterBubble;
	import bb.game.mode.LiveLong.Components.LLRegularBubble;
	import bb.game.mode.LiveLong.Components.LLDirtyBubble;
	import bb.engine.env.components.ISupportProjectable;
	import bb.sound.BasicSound;
	import flash.display.ColorCorrection;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import bb.game.mode.GameMode;
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GlobalEnvObjTable;
	import flash.utils.Timer;
	import flash.text.StyleSheet;
	import flash.net.URLLoader;
	import flash.display.Loader;
	/**
	 * ...
	 * @author ...
	 */
	public class LiveLongMode extends GameMode
	{
		/**
		 * Class that 
		 */
		private var modeLayers:Dictionary = new Dictionary();
		private var scoreHandler:ScoreHandler = null;
		
		//Constansts
		static public const LEVELBORDERS:String = "LEVELBORDERS";
		
		//Handles the injection of the bubbles
		private var bubbleInjector:BubbleInjector;
		private var bubbleFactory:BubbleFactory;
		
		//supports controlling game progression
		private var controlMethods:ControlMethods;
		
		private var currentLevel:int = 0;
		private var currentLevelBubblesDestroyed:int;
		private var currentLevelBubblesOnScreen:int;
		private var currentLevelBubbleThreshold:int;
		private var numBubblesToDestroyForLevelComplete:int;
		
		private var gameStrikes:int; //3 strikes your out
		private var gameStrikesLimit:int = 3;
		
		private var injectionTimer:Timer;
		private var inTheRedTimer:Timer;				
		
		private var loadLevelCover:Sprite;
		private var inTheRedCover:Sprite;
		
		private var backgroundColor:int = 0xD4E190;
		private var inTheRedColor:int = 0xFF0000;
		
		private var playAgainButton:Sprite = null;
		
		//Load sounds
		private var sndBubblePop:BasicSound;
		private var modeMusic:BasicSound;
		
		//Instruction array 
		var instructionArr:Array = new Array();
		
		public function LiveLongMode() 
		{
			super();
			
			//loadMode();
			preload();
		}
		
		//external resources
		private function preload():void {
			//Preload everything, forever loop in preload until everything loaded

			var l:Loader;
			l = new Loader();
			l.load(new URLRequest(".\\inst\\InstScn5.jpg"));
			//l.loaderInfo.addEventListener(Event.COMPLETE, screenLoaded);
			getGetGameLayer().addChild(l);
			
			l = new Loader();
			l.load(new URLRequest(".\\inst\\InstScn4.jpg"));
			//l.loaderInfo.addEventListener(Event.COMPLETE, screenLoaded);
			getGetGameLayer().addChild(l);
			
			l = new Loader();
			l.load(new URLRequest(".\\inst\\InstScn3.jpg"));
			//l.loaderInfo.addEventListener(Event.COMPLETE, screenLoaded);
			getGetGameLayer().addChild(l);
			
			l = new Loader();
			l.load(new URLRequest(".\\inst\\InstScn2.jpg"));
			//l.loaderInfo.addEventListener(Event.COMPLETE, screenLoaded);
			getGetGameLayer().addChild(l);
			
			l = new Loader();
			l.load(new URLRequest(".\\inst\\InstScn1.jpg"));
			//l.loaderInfo.addEventListener(Event.COMPLETE, screenLoaded);
			getGetGameLayer().addChild(l);
			
			//Show the next page button			
			instructionButton();
			/**/
		}
		
		private function instructionButton():void {
			
			var next:TextField = new TextField();
			next.text = "NEXT PAGE";

			var Button:Sprite = new Sprite();
			Button.graphics.beginFill(0x00FF00, 1);
			Button.graphics.drawRoundRect(0, 0, 100, 50, 10, 10);
			Button.graphics.endFill();
			Button.x = 400;
			Button.y = 550;
			Button.addEventListener(MouseEvent.CLICK, nextPage);
			
			var styles:StyleSheet = new StyleSheet();
            styles.setStyle(".sty", {color: "#FF00FF", fontFamily: "Georgia", fontSize: "14"});
			next.styleSheet = styles;
			next.htmlText = "<span class=\"sty\">NEXT</span>";
			next.selectable = false;
			
			Button.addChild(next);
			
			getModeMenuLayer().addChild(Button);
		}
		
		private function nextPage(e:Event):void {
			getGetGameLayer().removeChildAt(getGetGameLayer().numChildren - 1);
			
			if (getGetGameLayer().numChildren == 0){
				//getModeMenuLayer().removeChildAt(0);
				loadMode();
				getModeMenuLayer().removeChildAt(0);
			}
		}
		
		//Push the loaded screen onto the instruction book
		
		private function screenLoaded(e:Event):void {
			instructionArr.push(e.target);
		}
		
		
		private function loadMode():void 
		{	
			initModeSounds();
			
			//Load the game mode layers
			initModeLayers();
			
			//Load background graphics
			initBackGround();
			
			//Load the HUD Display
			initHUD();
			
			//Build the borders
			buildLevel();
			
			//build level cover
			buildLoadLevelCover();			
			
			//init
			initPlayAgainButton();
			
			//initialize mode controllers6
			this.bubbleInjector = new BubbleInjector(modeLayers[LayersID.BACKGROUND], 30, 10);
			this.controlMethods = new ControlMethods();			
			this.bubbleFactory = new BubbleFactory(this.controlMethods);
			
			//initialize game state 
			this.currentLevel = 5;
			this.currentLevelBubblesDestroyed = 0;
			this.currentLevelBubblesOnScreen = 0;
			
			//25 max bubbles on screen
			this.currentLevelBubbleThreshold = 20;
			
			this.gameStrikes = 0; 
			
			//Set up the inTheRedTimer
			inTheRedTimer= new Timer(5000, 1);
			inTheRedTimer.addEventListener(TimerEvent.TIMER,this.setStrike);
			inTheRedCover = makeTestCover(0xFF0000);
			
			startGame();
		}
		
		private function initModeSounds():void {
			
			//Load audio
			this.sndBubblePop = new BasicSound(".\\sound\\popWater.mp3");
			BubbleProperties.setSoundBubblePop(sndBubblePop);
			
			this.modeMusic = new BasicSound(".\\sound\\mode.mp3",true);
			
		}
		
		private function initPlayAgainButton(labelTxt:String = "Play Again"):void {
		
			playAgainButton = new Sprite();
			playAgainButton.graphics.beginFill(0x00FF00, 1);
			playAgainButton.graphics.drawRoundRect(0, 0, 100, 50, 10, 10);
			playAgainButton.graphics.endFill();
			
			var label:TextField = new TextField();
			var styles:StyleSheet = new StyleSheet();
            styles.setStyle(".sty", {color: "#FF00FF", fontFamily: "Georgia", fontSize: "14"});
			label.styleSheet = styles;
			label.htmlText = "<span class=\"sty\">"+labelTxt+"</span>";
			label.selectable = false;
			
			playAgainButton.addChild(label);
			//label.x = (playAgainButton.width / 2) + (label.width / 2);
			//label.y = (playAgainButton.height / 2) + (label.height / 2);
			
			playAgainButton.x = modeLayers[LayersID.BACKGROUND].width / 2;
			playAgainButton.y = modeLayers[LayersID.BACKGROUND].height / 2;
			
		}
		/**
		 * Makes the layers that will be used by the mode.
		 * HUD - DIRTYDUBBLE - OTHERBUBBLES - BACKGROUND
		 */
		private function initModeLayers():void {
			modeLayers[LayersID.HUD] = new Sprite;
			
			modeLayers[LayersID.DIRTYBUBBLE] = new Sprite;
			modeLayers[LayersID.OTHERBUBBLES] = new Sprite;
			modeLayers[LayersID.BACKGROUND] = new Sprite;
			
			//Add to the these layers to the game mode layer
			getGetGameLayer().addChild(modeLayers[LayersID.BACKGROUND]);
			getGetGameLayer().addChild(modeLayers[LayersID.OTHERBUBBLES]);
			getGetGameLayer().addChild(modeLayers[LayersID.DIRTYBUBBLE]);
			getGetGameLayer().addChild(modeLayers[LayersID.HUD]);
		}
		
		//Init background
		private function initBackGround():void {
			
			//Make 
			var width:Number = (modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageWidth;
			var height:Number = (modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageHeight;
			
			var backing:Sprite = new Sprite(); 
			backing.graphics.beginFill(backgroundColor, 1);
			backing.graphics.drawRect(0, 0, width, height);
			backing.graphics.endFill();
			
			(modeLayers[LayersID.BACKGROUND] as Sprite).addChild(backing);
			
			//Notify Bubble properties of the levels background plane for sizing
			//shouldn't be adding objects to it directly
			BubbleProperties.setGamePlane(backing);
		}
		
		//Build the HUD - Score : BUBCOUNT : Level: 
		private function initHUD():void {
			var scoreText:TextField = makeTestText();
			var bubbleCountText:TextField = makeTestText();
			var levelText:TextField = makeTestText();
			var strikesText:TextField = makeTestText();
			var countToLevelUp:TextField = makeTestText();
			
			scoreHandler = new ScoreHandler(scoreText, bubbleCountText,levelText,strikesText,countToLevelUp);
			
			//Set up area for score on screen
			//need a class hook to the score 
			buildTestHeading(scoreText, bubbleCountText, levelText, strikesText,countToLevelUp);	
		}
		
		//Cover splash screen when loading next level
		private function buildLoadLevelCover():void {
			
			loadLevelCover = new Sprite();
			
			var backing:Sprite = makeTestCover(0x223344);
			
			loadLevelCover.addChild(backing);
		}
		
		private function makeTestCover(color:int):Sprite {
			var w:Number = (modeLayers[LayersID.BACKGROUND] as Sprite).width;
			var h:Number = (modeLayers[LayersID.BACKGROUND] as Sprite).height;
			
			var backing:Sprite = new Sprite();
			backing.graphics.beginFill(color, 1);
			backing.graphics.drawRect(0, 0, w, h);
			backing.graphics.endFill();
			
			return backing;
		}
		
		//Test Text
		private function makeTestText():TextField {
			//Make text how ever you want
			return new TextField();
		}
		
		//Build the heading for the HUD. Score, Level, etc
		private function buildTestHeading(scoreText:TextField, bubbleCountText:TextField, levelText:TextField, strikesText:TextField, countToLevelUpText:TextField):void {
			
			var scoreLbl:TextField = makeTestText();
			var bubbleLbl:TextField = makeTestText();
			var levelLbl:TextField = makeTestText();
			var countToNextLevelLbl:TextField = makeTestText();
			
			scoreLbl.text = "SCORE:"
			bubbleLbl.text = "BUBCOUNT:"
			levelLbl.text = "LEVEL:"
			countToNextLevelLbl.text = "COUNTDOWN"
			
			scoreText.text = "ZERO";
			bubbleCountText.text = "ZERO";
			levelText.text = "ZERO";
			strikesText.text = "-";
			countToLevelUpText.text = "-";
			
			var width:Number = (this.modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageWidth;
			var height:Number = (this.modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageHeight;
			
			//Positions Labels on Screen
			scoreLbl.x = width * .05;
			scoreLbl.y = height * .01;
			
			bubbleLbl.x = width * .20;
			bubbleLbl.y = height * .01;
			
			levelLbl.x = width * .5;
			levelLbl.y = height * .01;
			
			countToNextLevelLbl.x = width * .7;
			countToNextLevelLbl.y = height * .01;
			
			strikesText.x = width * .95;
			strikesText.y = width * .01;
			
			//Positions Text
			scoreText.x = scoreLbl.x + (scoreLbl.width/2);
			scoreText.y = scoreLbl.y;
			
			bubbleCountText.x = bubbleLbl.x + (bubbleLbl.width);
			bubbleCountText.y = bubbleLbl.y;
			
			levelText.x = levelLbl.x + (levelLbl.width);
			levelText.y = levelLbl.y;
			
			countToLevelUpText.x = countToNextLevelLbl.x + (countToNextLevelLbl.width);
			countToLevelUpText.y = countToNextLevelLbl.y;
			
			//Fix the height on the labels
			var h:int = 18;
			bubbleLbl.height = h;
			scoreLbl.height = h;
			levelLbl.height = h;
			countToNextLevelLbl.height = h;
			scoreText.height = h;
			bubbleCountText.height = h;
			levelText.height = h;
			strikesText.height = h;
			countToLevelUpText.height = h;
			
			//Push to screen
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(scoreLbl);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(bubbleLbl);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(scoreText);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(bubbleCountText);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(levelLbl);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(levelText);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(strikesText);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(countToNextLevelLbl);
			(this.modeLayers[LayersID.HUD] as Sprite).addChild(countToLevelUpText);	
		}
		
		//Place the borders on the background
		private function buildLevel():void {
			
			//get background layer
			var background:Sprite = this.modeLayers[LayersID.BACKGROUND] as Sprite;
			
			//get the madde borders
			var borders:Array = getBorders(0x000000, 10, 1);
			
			//add the levels boards to the global table for central access
			GlobalEnvObjTable.getInstance().addEnvObjs(LEVELBORDERS, borders);
			
			//Add the borders to the background
			borders.forEach(function(border, ind, theBorders) { 
				background.addChild(border);				
			});
			
			//BubbleProperties need to notified of what borders to build against
			BubbleProperties.setStageBorders(borders);
		}
		
		//gets stage borders to use
		private function getBorders(color:uint,thickness:Number, alpha:Number = 1):Array {
			
			var stageWidth:Number = (this.modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageWidth;
			var stageHeight:Number = (this.modeLayers[LayersID.BACKGROUND] as Sprite).stage.stageHeight;
			
			var borders:Array = new Array();
			
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(color, alpha);
			upSide.graphics.drawRect(0, 0, stageWidth, thickness);
			upSide.graphics.endFill();
			borders.push(upSide);
			
			var leftSide:EnvObj = new EnvObj();
			leftSide.graphics.beginFill(color, alpha);
			leftSide.graphics.drawRect(0, 0, thickness, stageHeight);
			leftSide.graphics.endFill();
			borders.push(leftSide);
			
			var rightSide:EnvObj = new EnvObj();
			rightSide.graphics.beginFill(0x000000, 1.0);
			rightSide.graphics.drawRect(0, 0, thickness, stageHeight);
			rightSide.graphics.endFill();
			rightSide.x = stageWidth - rightSide.width;
			borders.push(rightSide);
			 
			var bottomSide:EnvObj = new EnvObj();
			bottomSide.graphics.beginFill(0x000000, 1.0);
			bottomSide.graphics.drawRect(0, 0, stageWidth, thickness);
			bottomSide.graphics.endFill();
			bottomSide.y = stageHeight - bottomSide.height;                    
			borders.push(bottomSide); 

			return borders;
		}
		
		//start the game
		private function startGame():void {
			
			//runs for 5 minutes based on 1 sec intervals
			
			//need to register a method here that is notified when a bubble is destroyed
			BubbleProperties.regBubbleDestroyed(this.onBubbleDestroyed);
			BubbleProperties.counterBubbleMerged(this.onBubbleMerged);
			
			//load the first level
			loadLevel();
		}
		
		//load the level
		private function loadLevel():void {		
			//Draw the 
			(modeLayers[LayersID.HUD] as Sprite).addChild(loadLevelCover);
			
			//Show the splash
			var t:Timer = new Timer(1000/4, 1);
			t.addEventListener(TimerEvent.TIMER, levelLoaded);
			t.start();
			
			doLoadLevel();				
		}
		
		//When the level loaded remove splash screen
		private function levelLoaded(e:Event) {
			loadLevelCover.parent.removeChild(loadLevelCover);
		}
		
		//load the next level
		private function doLoadLevel(e:TimerEvent = null):void {
			
			var currentLevel:int = this.currentLevel;
			
			//Reset injection time
			if (injectionTimer != null)				
				injectionTimer.removeEventListener(TimerEvent.TIMER, doInject);			

			this.injectionTimer = new Timer(this.controlMethods.calcBubbleInjectionInterval(this.currentLevel), 0);
			this.injectionTimer.addEventListener(TimerEvent.TIMER, doInject);
			this.injectionTimer.start();
			
			//Mark the screen with the level
			this.scoreHandler.setLevel(currentLevel);
			
			//clear all remaining bubbles off the screen and destroy
			this.clearAllBubblesFromScreen();
			
			//get the number of bubbles to initially splash on stage 
			var numBubblesToStart:int = controlMethods.calcNumBubblesToStartLevel(currentLevel);
			this.currentLevelBubblesOnScreen = numBubblesToStart;
			this.scoreHandler.setBubbleCount(this.currentLevelBubblesOnScreen);
			
			//reset the num of bubbles destroyed
			this.currentLevelBubblesDestroyed = 0;
			
			//places these bubbles on the screen.
			initSplashScreenLevelStart(numBubblesToStart);
			
			//get number of bubbles that should be destroyed for level up
			this.numBubblesToDestroyForLevelComplete = controlMethods.calcNumBubblesToPopForLevelComplete(currentLevel);
			scoreHandler.setCountToLevelUp(this.numBubblesToDestroyForLevelComplete);
		}
		
		//Clear all the bubbles on the screen
		private function clearAllBubblesFromScreen():void {
			
			var outArr:Array;
			
			outArr = GlobalEnvObjTable.getInstance().getEnvObjs(LLCounterBubble.ID);			
			if(outArr != null)
				callDisposeOnBubbles(outArr);
			
			outArr = GlobalEnvObjTable.getInstance().getEnvObjs(LLRegularBubble.ID);
			if(outArr != null)
				callDisposeOnBubbles(outArr);
			
			outArr = GlobalEnvObjTable.getInstance().getEnvObjs(LLDirtyBubble.ID);
			if(outArr != null)
				callDisposeOnBubbles(outArr);
		}
		
		//Calls destroy on Counter,Regular and Dirty Bubbles in GlobalEnvTable
		private function callDisposeOnBubbles(envObjs:Array) {

			//use
			while (envObjs.length != 0) {
				(envObjs[0] as Bubble).dispose();
			}
			
		}
		
		private function onBubbleDestroyed(e:Event = null):void {
			
			//figure out what bubble destroyed
			var prop:EnvObjProperty = e.target as EnvObjProperty;
			var envObj:EnvObj = prop.getEnvObj();
			
			if (envObj is LLRegularBubble) {
				//add to score				
				scoreHandler.updateScore(scoreHandler.getScore() + 1);
				
				//if number of bubbles for level destroyed
				//load next level
			}else if (envObj is LLCounterBubble) {
				var llcb:LLCounterBubble =  envObj as LLCounterBubble;
				
				//get the current count out of the counter bubble
				var count:int = llcb.getCount();
				scoreHandler.updateScore(scoreHandler.getScore() + count);
				
				//Add the count to the number of bubbles on screen
				//This is because the counter bubble has bursted
				if(count > 0){
					this.currentLevelBubblesOnScreen += count; 
					scoreHandler.setBubbleCount(this.currentLevelBubblesOnScreen);
				}
				
			}	
			//work around...cant dispose until this callback was called
			//(envObj as Bubble).dispose();
			
			//Increment the amount of bubbles on screen destroyed
			this.currentLevelBubblesDestroyed++;
			scoreHandler.setCountToLevelUp(this.numBubblesToDestroyForLevelComplete - this.currentLevelBubblesDestroyed);
			
			//A bubble was destroyed
			this.currentLevelBubblesOnScreen--;
			this.scoreHandler.decBubbleCount();
			
			if (this.currentLevelBubblesDestroyed >= this.numBubblesToDestroyForLevelComplete) {
				//load the next level
				this.currentLevel++;
				loadLevel();
				//doLoadLevel();
			}
			
			attemptToSetStrike();
		}
		
		private function onBubbleMerged(e:Event = null):void {
			
			var llcb:LLCounterBubble = ((e.target as EnvObjProperty).getEnvObj() as LLCounterBubble);
		
			//get the current count out of the counter bubble
			var num:int = llcb.getCount();
			
			//add the points to the buuble after merge
			scoreHandler.updateScore(scoreHandler.getScore() + Math.abs(num));
			
			//Should have one less bubble on screen, update the count
			this.currentLevelBubblesOnScreen--;
			this.scoreHandler.decBubbleCount();
			
			//Bubbles left to burst for current level
			scoreHandler.setCountToLevelUp(this.numBubblesToDestroyForLevelComplete - this.currentLevelBubblesDestroyed);
			
			attemptToSetStrike();
		}
		
		//Place bubbles randomly on screen
		private function initSplashScreenLevelStart(numBubblesToCreate:int):void {
			
			for (var i:int = 0; i < numBubblesToCreate; i++) 
			{
				//get type of bubble to create
				var bubble:Bubble = bubbleFactory.getRandomBubble(this.currentLevel);
				
				//place the bubble on screen
				bubbleInjector.randomStagePlacement(bubble);
				
				//Set bub speed and mag
				var bubSpeed:Number = this.controlMethods.calcBubbleSpeed(this.currentLevel);
				var bubAng:Number = Math.random() * 360;
				bubble.getIProjectable().setProjection(bubAng, bubSpeed);
				
				//add child to visible layer
				if (bubble.getBubbleClassID() == LLDirtyBubble.ID){
					(modeLayers[LayersID.DIRTYBUBBLE] as Sprite).addChild(bubble);
					
					//Get size
					var scale:Number = controlMethods.calcDirtySize(this.currentLevel);
					bubble.scaleX = scale;
					bubble.scaleY = scale;
				}
				else {
					
					//Count up or down
					if (bubble.getBubbleClassID() == LLCounterBubble.ID) {
						(bubble as LLCounterBubble).countUp(controlMethods.counterDirectionUp());
					}
					
					(modeLayers[LayersID.OTHERBUBBLES] as Sprite).addChild(bubble);
				}
			}
			
		}
		
		//Injects another bubble into the screen
		private function doInject(e:TimerEvent):void {
			
			/*
			//Get random bubble to inject
			var bubble:Bubble = bubbleFactory.getRandomBubble(this.currentLevel);
			
			//Set magnitude
			var bubspeed:Number = controlMethods.calcBubbleSpeed(this.currentLevel);
			(bubble as ISupportProjectable).getIProjectable().setProjection(0, bubspeed);
			
			//random counter direction
			if (bubble.getBubbleClassID() == LLCounterBubble.ID) {
				(bubble as LLCounterBubble).countUp(controlMethods.counterDirectionUp());
			}
						
			//Add the bubble to the screen
			addBubbleToScreen(bubble);
			
			//Inject the bubble in the playing field
			this.bubbleInjector.randomSideInject(bubble as ISupportProjectable);
			*/
			
			initSplashScreenLevelStart(1);
			
			//Increase the number of bubbles on the screen as it is injected
			this.currentLevelBubblesOnScreen++;
			this.scoreHandler.incBubbleCount();
			
			//Do checks and actions on if strike should be set
			attemptToSetStrike();
		}
		
		private function attemptToSetStrike():void {
			if (this.currentLevelBubblesOnScreen > this.currentLevelBubbleThreshold){				
				//Play hurry up sound
				
				//Turn background red because over limit
				toggleBackgroundToRed(true);
				//inTheRedTimer.reset();
				inTheRedTimer.start();
			}else {
				//stop timer .. set background to default
				inTheRedTimer.reset();
				toggleBackgroundToRed(false);
			}
		}
		
		private function setStrike(e:Event):void {			
			this.gameStrikes++;
			this.scoreHandler.setStrikes(this.gameStrikes);
			
			attemptToSetStrike();
			
			if (this.gameStrikes == 3) {
				resetGame();
			}	
		}
		
		private function resetGame():void {
			//Clear out the envObjTable
			clearAllBubblesFromScreen();
	
			(modeLayers[LayersID.HUD] as Sprite).addChild(playAgainButton);
			playAgainButton.addEventListener(MouseEvent.CLICK, playAgain);	
			
			injectionTimer.stop();
			injectionTimer.removeEventListener(TimerEvent.TIMER, this.doInject);			
			inTheRedTimer.removeEventListener(TimerEvent.TIMER,this.setStrike);
			//t.addEventListener(TimerEvent.TIMER, levelLoaded);
			
			modeMusic.stopSound();
		}
		
		private function playAgain(e:Event = null):void {
			GlobalEnvObjTable.getInstance().destroyObjectsViaPM();
			playAgainButton.removeEventListener(MouseEvent.CLICK, playAgain);
			resetGameLayer();
			loadMode();
		}

		private function toggleBackgroundToRed(turnRed:Boolean):void {
			if (turnRed) {
				(modeLayers[LayersID.BACKGROUND] as Sprite).addChild(inTheRedCover);
			}else {
				if(inTheRedCover.parent != null)
					inTheRedCover.parent.removeChild(inTheRedCover);
			}
		}
		
		private function whenOverBubblesOnScreenThreshold():void {		}
		
		/**
		 * TODO when the bubble is created , it needs to be added to the EnvObjTable 
		 * @param	envObj
		 */
		private function addBubbleToScreen(envObj:EnvObj):void {
			if (envObj is LLDirtyBubble) {
				(modeLayers[LayersID.DIRTYBUBBLE] as DisplayObjectContainer).addChild(envObj);
			}else {
				(modeLayers[LayersID.OTHERBUBBLES] as DisplayObjectContainer).addChild(envObj);
			}
		}
		
		private function tryAgainScreen(e:Event):void {
			
			
			
		}
		
		/*
		 * private function showTweet(tweet:StatusData):void
        {
            var bg:Sprite = new Sprite();
            bg.graphics.lineStyle(1, 0x888888,1,true);
            bg.graphics.beginFill(0,.4);
            bg.graphics.drawRoundRect(0, 0, 350, 200, 15, 15);
            bg.graphics.endFill();
            bg.filters = [new DropShadowFilter(7, 45, 0, .6, 4, 4, 1, 1)]; 
            
            var styles:StyleSheet = new StyleSheet();
            styles.setStyle(".tweet", {color: "#FFFFFF", fontFamily: "Georgia", fontSize: "12"});
            styles.setStyle(".age", {fontStyle: "italic", fontSize: "10"});
            
            var textField:TextField = new TextField();
            textField.styleSheet = styles;
            textField.width = 310;
            textField.wordWrap = true;
            textField.multiline = true;
            textField.htmlText = "<img src=\""+tweet.user.profileImageUrl+"\" height=\"50\" width=\"50\">" + 
                                 "<p class=\"tweet\"><b>" + 
                                 "<a target=\"_blank\" href=\"http://twitter.com/"+tweet.user.screenName+"\">@"+tweet.user.screenName+"</a></b> " + 
                                 ""+tweet.text + "" + 
                                 "<br><span class=\"age\">"+TweetUtil.returnTweetAge(tweet.createdAt)+"</span></p>";
            
            bg.height = (textField.height < 100) ? 120 : textField.height + 20;
            bg.x = stage.stageWidth/2 - bg.width/2;
            bg.y = stage.stageHeight/2 - bg.height/2;
            textField.x = bg.x + 20;
            textField.y = bg.y + 20;
            
            addChild(bg);
            addChild(textField);
        }
		*/
	}

}