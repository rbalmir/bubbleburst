package bb.game.mode.LiveLong 
{
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.text.TextField
	/**
	 * ...
	 * @author ...
	 */
	public class ScoreHandler 
	{
		private var scoreField:TextField;
		private var bubbleCountField:TextField;
		private var countToLevelUpField:TextField;
		private var levelField:TextField;
		private var strikesField:TextField;
		
		private var bubbleCount:int;
		private var score:int;
		
		public function ScoreHandler(scoreField:TextField, bubbleCount:TextField, currentLevel:TextField = null, strikes:TextField = null, countToLevelUp:TextField = null) 
		{
			this.scoreField = scoreField;
			this.bubbleCountField = bubbleCount;
			
			this.levelField = currentLevel;	
			this.strikesField = strikes;
			
			this.countToLevelUpField = countToLevelUp;
		}
		
		public function updateScore(score:int):void {
			scoreField.text = score.toString();
			this.score = score;
		}
		
		public function getScore():int {
			return this.score;
		}
		
		public function setCountToLevelUp(count:int):void {
			countToLevelUpField.text = count.toString();
		}
		
		public function setBubbleCount(count:int):void {
			this.bubbleCount = count;
			bubbleCountField.text = count.toString();
		}
		
		public function getBubbleCount():int {
			return this.bubbleCount;
		}
		
		public function incBubbleCount(e:Event = null):void {
			bubbleCount++;
			bubbleCountField.text = bubbleCount.toString();
		}
		
		public function decBubbleCount(e:Event = null):void {
			bubbleCount--;
			bubbleCountField.text = bubbleCount.toString();
		}
		
		public function setLevel(level:int):void {
			this.levelField.text = level.toString();
		}
		
		public function setStrikes(strikes:int) {
			trace("Set strikes:" + strikes);
		switch(strikes){
			case 1:
				strikesField.text = "X";
				break;
			case 2:
				strikesField.text = "XX";				
				break;				
			case 3:
				strikesField.text = "XXX";
				break;
		}
		
		}
		
	}

}