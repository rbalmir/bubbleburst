package bb.movie 
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	/**
	 * Simple MovieClip player intended to host the playing of standin 
	 * animations.  
	 * @author Rich Balmir
	 */
	public class BasicMovie implements IPlayMovie 
	{
		private var mc:MovieClip = null;
		
		public function BasicMovie() 
		{	}
		
		/**
		 * Set the movieclip intended to serve as the standin animation handler
		 * @param	mc
		 */
		public function setMovieClip(mc:MovieClip) {
			this.mc = mc;
		}
		
		/**
		 * Get the stand in movie as a display object
		 * @return
		 */
		public function getMovie():DisplayObject {
			return mc;
		}
		
		/**
		 * Play the movie
		 */
		public function playMovie():void {
			if (mc != null)
				mc.play();
		}
		
		/**
		 *  Stop the movie
		 */
		public function stopMovie():void {
			if (mc != null)
				mc.stop();
		}
	}

}