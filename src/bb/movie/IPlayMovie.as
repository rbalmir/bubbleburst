package bb.movie 
{
	import flash.display.DisplayObject;
	
	/**
	 * Simple animation player interface.  Intended to allow a animation to play
	 * on its own terms.  Hides if movieclip or sprite based timed is used.
	 * ...
	 * @author Rich Balmir
	 */
	public interface IPlayMovie 
	{
		function playMovie():void;
		function stopMovie():void;
		function getMovie():DisplayObject;	//Gets the root display object that represents animation
											//Needed for client to place animation on display list
	}
	
}