package bb.sound 
{
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class BasicSound implements IPlaySound 
	{
		var soundClip:Sound;
		var finishLoading:Boolean = false;
		var loop:Boolean = false;
		var sndChannel:SoundChannel;

		//var callback:Function = null;
		public function BasicSound(url:String, loop:Boolean = false) 
		{
			this.loop = loop;
			
			//Create an instance of the Sound class
			soundClip=new Sound();
			
			//Create a new SoundChannel Object
			sndChannel = new SoundChannel();
			
			//Load sound using URLRequest
			soundClip.load(new URLRequest(url));
			
			//Create an event listener that wll update once sound has finished loading
			soundClip.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
		}
		
		/* INTERFACE bb.sound.IPlaySound */
		public function playSound():void 
		{
			if (loop == true) {
				sndChannel = soundClip.play();
				sndChannel.addEventListener(Event.SOUND_COMPLETE, this.loopMeth);
			}else {
				soundClip.play();	
			}
			
		}
		
		private function loopMeth(e:Event) {
			//if(sndChannel != null)
			//	sndChannel.removeEventListener(Event.SOUND_COMPLETE, this.loopMeth);
			
			sndChannel = soundClip.play();
			sndChannel.addEventListener(Event.SOUND_COMPLETE, this.loopMeth);
		}
		
		public function stopSound():void 
		{
			sndChannel.stop();
			sndChannel.removeEventListener(Event.SOUND_COMPLETE, this.loopMeth);
		}
		
		private function onComplete(evt:Event):void {
			//Play loaded sound
			trace("Sound Loaded");
			
			sndChannel = soundClip.play();
				
			if (loop == true) {
				sndChannel.addEventListener(Event.SOUND_COMPLETE, this.loopMeth);
			}	

		}
		
	}

}