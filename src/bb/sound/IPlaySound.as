package bb.sound 
{
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public interface IPlaySound 
	{
		function playSound():void;
		function stopSound():void;
	}
	
}