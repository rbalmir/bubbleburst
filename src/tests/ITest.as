package tests 
{
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public interface ITest 
	{
		function testName():String;
		function testDescription():String;
		function test():void;		
	}
	
}