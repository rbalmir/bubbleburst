package tests 
{
	import bb.engine.env.components.EnvObj;

	import bb.engine.env.components.properties.Touchable;	
	import bb.engine.env.components.properties.MouseClicked;
	
	import bb.game.components.properties.BasicBubblePop;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class TestBasicBubblePop extends Sprite implements ITest 
	{
		
		public function TestBasicBubblePop (baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "BasicBubblePop";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Init regular and counter bubbles
			var regBubble:EnvObj = addBubbleToMainScreen();
			regBubble.x = 415;
			regBubble.y = 415;
			
			var cntBubble1:EnvObj = addCounterBubbleToMainScreen(0x00FF00);
			cntBubble1.x = 415;
			cntBubble1.y = 445;
			
			//Config a Touchable MouseClick to Bubble
 			var regBubClick1:Touchable = new MouseClicked();
			regBubClick1.setEnvObj(regBubble);
			regBubClick1.activate();	
			
			//Wire a Basic Bubble Pop to a mouse click event
			var basicBubblePop:BasicBubblePop = new BasicBubblePop();
			regBubClick1.addEventListener(regBubClick1.getPropertyID(), basicBubblePop.invokeProperty);
			basicBubblePop.setEnvObj(regBubble);
			basicBubblePop.activate();
			
		}
		
		private function initBubblesOnScreen():void {
			
		}
		
		//Adds bubble to main sprite
		private function addBubbleToMainScreen():EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(0x00FFFF,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		private function addCounterBubbleToMainScreen(color:uint):EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(color,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		

		
		
	}

}