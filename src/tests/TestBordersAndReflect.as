package tests
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.*;
	
	import flash.display.Sprite;
	
	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestBordersAndReflect extends Sprite implements ITest
	{
		
		public function TestBordersAndReflect(baseSprite:Sprite)
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String
		{
			return "CounterBubble";
		}
		
		public function testDescription():String
		{
			return "";
		}
		
		public function test():void
		{
			//Init stage
			StageObjects.setStageBorders(getBorders());
			StageObjects.setGameLayer(this);
			
			//Init regular and counter bubbles
			var cntBubble:CounterBubble = new CounterBubble();
			
			cntBubble.x = 600;
			cntBubble.y = 415;
			
			//cntBubble.getIProjectable().setProjection(180, 4);
			//addChild(cntBubble);
			
			//--------
			
			cntBubble = new CounterBubble();
			
			cntBubble.x = 100;
			cntBubble.y = 415;
			
			cntBubble.getIProjectable().setProjection(0, 3);
			addChild(cntBubble);
			
		}
		
		private function getBorders():Array
		{
			var borders:Array = new Array();
			
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(0x000000, 1.0);
			upSide.graphics.drawRect(0, 0, 800, 7);
			upSide.graphics.endFill();
			addChild(upSide);
			borders.push(upSide);
			
			var leftSide:EnvObj = new EnvObj();
			leftSide.graphics.beginFill(0x000000, 1.0);
			leftSide.graphics.drawRect(200, 0, 7, 800);
			leftSide.graphics.endFill();
			addChild(leftSide);
			borders.push(leftSide);
			
			var rightSide:EnvObj = new EnvObj();
			rightSide.graphics.beginFill(0x000000, 1.0);
			rightSide.graphics.drawRect(793, 0, 7, 800);
			rightSide.graphics.endFill();
			addChild(rightSide);
			borders.push(rightSide);
			
			var bottomSide:EnvObj = new EnvObj();
			bottomSide.graphics.beginFill(0x000000, 1.0);
			bottomSide.graphics.drawRect(0, 594, 800, 7);
			bottomSide.graphics.endFill();
			addChild(bottomSide);
			borders.push(bottomSide);
			
			return borders;
		}
	
	}

}