package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.game.components.*;
	import bb.engine.env.components.properties.LinearMovement;
	import flash.events.MouseEvent;
	//import fl.controls.TextArea;
	//import fl.controls.TextInput;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class TestBubblesAndBorders extends Sprite implements ITest 
	{
		var angle:int;
		var mag:int;
		var angH:int;
		var magH:int;
		var linMove:LinearMovement = new LinearMovement();
		var txt:TextField = new TextField();
		var txt2:TextField = new TextField();
		var resetB:Sprite = new Sprite();
		var enterB:Sprite = new Sprite();
		var posx:int = 400;
		var posy:int = 300;
		
		var leftSide:Sprite = new Sprite();
		var rightSide:Sprite = new Sprite();
		var bottomSide:Sprite = new Sprite();
		var upSide:Sprite = new Sprite();
		var hole:Sprite = new Sprite();
		var bubble:EnvObj = addBubbleToMainScreen();
		var tryUp:Sprite = new Sprite();
		var tryDo:Sprite = new Sprite();
		var tryLe:Sprite = new Sprite();
		var tryRi:Sprite = new Sprite();
		
		var difUp:Sprite = new Sprite();
		var difDo:Sprite = new Sprite();
		public function TestBubblesAndBorders(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "BUBBBLESANDBORDERS"
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			getBorders();
			
			//Add a bubble to the main screen and retrieve it
			var bubble:EnvObj = addBubbleToMainScreen();

			//Initialize Linear Movements 
			var linMove:LinearMovement = new LinearMovement();
			linMove.setEnvObj(bubble);
			linMove.setProjection(90, 10);
			linMove.setPosition(new Point(0, 300));
			linMove.activate();
			
			//Set up the stage borders
			StageObjects.setStageBorders(getBorders());
			
			//Configure Collision Property
			var collHandler:BubbleStageBorderCollision = new BubbleStageBorderCollision();
			//collHandler.setProjectorHandler(linMove);
			//Register coll handler for move events
			linMove.addEventListener(linMove.getPropertyID(), collHandler.checkCollisionEvent);
			collHandler.setWatchEnvObjs(getBorders());			//Passing in the borders as an array
			collHandler.setEnvObj(bubble);
			collHandler.activate();
		}
		
		private function getBorders():Array {
			var borders:Array = new Array();
			
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(0x000000, 1.0);
			upSide.graphics.drawRect(0, 0, 800, 7);
			upSide.graphics.endFill();
			addChild(upSide);
			borders.push(upSide);
			
			var leftSide:EnvObj = new EnvObj();
			leftSide.graphics.beginFill(0x000000, 1.0);
			leftSide.graphics.drawRect(0, 0, 7, 800);
			leftSide.graphics.endFill();
			addChild(leftSide);
			borders.push(leftSide);
			
			var rightSide:EnvObj = new EnvObj();
			rightSide.graphics.beginFill(0x000000, 1.0);
			rightSide.graphics.drawRect(793, 0, 7, 800);
			rightSide.graphics.endFill();
			addChild(rightSide);
			borders.push(rightSide);
			
			var bottomSide:EnvObj = new EnvObj();
			bottomSide.graphics.beginFill(0x000000, 1.0);
			bottomSide.graphics.drawRect(0, 594, 800, 7);
			bottomSide.graphics.endFill();
			addChild(bottomSide);
			borders.push(bottomSide);
			
			return borders;
		}
		
		//Adds bubble to main sprite
		private function addBubbleToMainScreen():EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(0x00FFFF,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		private function changeAngle():void
		{
			angH = parseInt(txt.text);
			angle = angH;
			trace(angle);
			linMove.setProjection(angle, mag);
			trace("call me");
		}
		
		private function changeMag():void
		{
			magH = parseInt(txt2.text);
			mag = magH;
			linMove.setProjection(angle, mag);
		}
		
		private function clicked(event:MouseEvent):void
		{
			angle = 0;
			mag = 0;
			linMove.setProjection(angle, mag);
			linMove.setPosition(new Point(posx, posy));
		}
		
		private function enterV(event:MouseEvent):void
		{
			changeAngle();
			changeMag();
		}
		
		function collide (event:Event):void
		{
			if (bubble.hitTestObject(hole))
			{
				trace("Winner");
				angle = 0;
				mag = 0;
				linMove.setProjection(angle, mag);
				linMove.setPosition(new Point(posx, posy));
				hole.height -= 10;
				hole.width -= 10;
			}
		}
		
			function moveUp (event:Event):void
			{
				angle = 0;
				linMove.setProjection(angle, mag);
			}
			
			function moveDo (event:Event):void
			{
				angle = 180;
				linMove.setProjection(angle, mag);
			}
			
			function moveLe (event:Event):void
			{
				angle = 270;
				linMove.setProjection(angle, mag);
			}
			
			function moveRi (event:Event):void
			{
				angle = 90;
				linMove.setProjection(angle, mag);
			}
		
			function difUp1 (event:Event):void
			{
				mag += 1;
				linMove.setProjection(angle, mag);
			}
			
			function difDo1 (event:Event):void
			{
				mag -= 1;
				linMove.setProjection(angle, mag);
			}
	}

}