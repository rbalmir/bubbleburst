package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.CounterBubble;
	import flash.display.Sprite;

	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching 
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestCounterBubble extends Sprite implements ITest 
	{
		
		public function TestCounterBubble(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "CounterBubble";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Init regular and counter bubbles
			var cntBubble:CounterBubble = new CounterBubble();
			
			cntBubble.x = 600;
			cntBubble.y = 415;
			
			cntBubble.getIProjectable().setProjection(270, 1);
			addChild(cntBubble);
			
			//--------
			cntBubble = new CounterBubble();
			
			cntBubble.x = 100;
			cntBubble.y = 415;
			
			cntBubble.getIProjectable().setProjection(90, 3);
			addChild(cntBubble);

			
		}
			
	}

}