package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GlobalEnvObjTable;
	
	import bb.engine.env.components.properties.Collisionable;
	import bb.engine.env.components.properties.GlobalCollisionCheck;
	import bb.game.components.properties.CounterBubbleMerge;
	import bb.engine.env.components.properties.Touchable;	
	import bb.engine.env.components.properties.MouseClicked;
	import bb.game.components.properties.BasicBubblePop;
	
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching 
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestCounterBubbleMergeThenPop extends Sprite implements ITest 
	{
		
		public function TestCounterBubbleMergeThenPop(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "CounterBubbleMerge";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Init regular and counter bubbles
			var regBubble:EnvObj = addBubbleToMainScreen();
			regBubble.x = 415;
			regBubble.y = 415;
			
			var cntBubble1:EnvObj = addCounterBubbleToMainScreen(0x00FF00);
			cntBubble1.x = 415;
			cntBubble1.y = 445;
			
			var cntBubble2:EnvObj = addCounterBubbleToMainScreen(0x00EF00);
			cntBubble2.x = 445;
			cntBubble2.y = 445;
			
			var cntBubble3:EnvObj = addCounterBubbleToMainScreen(0x00DF00);
			cntBubble3.x = 445;
			cntBubble3.y = 415;
			
			//Register counter bubbles to global on screen 
			var globalTable:GlobalEnvObjTable = GlobalEnvObjTable.getInstance();
			var globalCntBubblesID = "CounterBubbles";
			
			//Register bubbles to globalTable
			globalTable.addEnvObj(globalCntBubblesID, cntBubble1);
			globalTable.addEnvObj(globalCntBubblesID, cntBubble2);
			globalTable.addEnvObj(globalCntBubblesID, cntBubble3);
			
			//Conifg a Touchable MouseClick to Bubble
			//------------------------------------------
 			var cntBubClick1:Touchable = new MouseClicked();
			cntBubClick1.setEnvObj(cntBubble1);
			cntBubClick1.activate();
			
			var cntBubClick2:Touchable = new MouseClicked();
			cntBubClick2.setEnvObj(cntBubble2);
			cntBubClick2.activate();
			
			var cntBubClick3:Touchable = new MouseClicked();
			cntBubClick3.setEnvObj(cntBubble3);
			cntBubClick3.activate();
			
			//Generate Global Collision Check Handler
			//------------------------------------------------------
			var globalCollCheckProp1:GlobalCollisionCheck = new GlobalCollisionCheck(globalCntBubblesID);
			globalCollCheckProp1.setEnvObjCollectionID(globalCntBubblesID);
			//cntBubClick1.addEventListener(cntBubClick1.getPropertyID(), globalCollCheckProp1.checkCollisionEvent);
			globalCollCheckProp1.setEnvObj(cntBubble1);
			globalCollCheckProp1.activate();
			
			var globalCollCheckProp2:GlobalCollisionCheck = new GlobalCollisionCheck(globalCntBubblesID);
			globalCollCheckProp2.setEnvObjCollectionID(globalCntBubblesID);
			//cntBubClick2.addEventListener(cntBubClick2.getPropertyID(), globalCollCheckProp1.checkCollisionEvent);
			globalCollCheckProp2.setEnvObj(cntBubble2);
			globalCollCheckProp2.activate();
			
			var globalCollCheckProp3:GlobalCollisionCheck = new GlobalCollisionCheck(globalCntBubblesID);
			globalCollCheckProp3.setEnvObjCollectionID(globalCntBubblesID);
			//cntBubClick3.addEventListener(cntBubClick3.getPropertyID(), globalCollCheckProp1.checkCollisionEvent);
			globalCollCheckProp3.setEnvObj(cntBubble3);
			globalCollCheckProp3.activate();
		
			//Wire CounterBubbleMerge To a mouse click and aggregate a Collisionable checker
			//-------------------------------------------------------------------------------
			var cntBubbleMergeProp1:CounterBubbleMerge = new CounterBubbleMerge(globalCollCheckProp1);	//create merge prop
			cntBubbleMergeProp1.setCollisionCheckHandler(globalCollCheckProp1);
			cntBubClick1.addEventListener(cntBubClick1.getPropertyID(), cntBubbleMergeProp1.invokeProperty);
			//globalCollCheckProp1.addEventListener(globalCollCheckProp1.getPropertyID(), cntBubbleMergeProp1.invokePropertyFromEvent);
			cntBubbleMergeProp1.setEnvObj(cntBubble1);	
			cntBubbleMergeProp1.activate();
			
			var cntBubbleMergeProp2:CounterBubbleMerge = new CounterBubbleMerge(globalCollCheckProp2);	//create merge prop
			cntBubbleMergeProp2.setCollisionCheckHandler(globalCollCheckProp2);
			cntBubClick2.addEventListener(cntBubClick2.getPropertyID(), cntBubbleMergeProp2.invokeProperty);
			//globalCollCheckProp2.addEventListener(globalCollCheckProp2.getPropertyID(), cntBubbleMergeProp2.invokePropertyFromEvent);
			cntBubbleMergeProp2.setEnvObj(cntBubble2);	
			cntBubbleMergeProp2.activate();
			
			var cntBubbleMergeProp3:CounterBubbleMerge = new CounterBubbleMerge(globalCollCheckProp3);	//create merge prop
			cntBubbleMergeProp3.setCollisionCheckHandler(globalCollCheckProp3);
			cntBubClick3.addEventListener(cntBubClick3.getPropertyID(), cntBubbleMergeProp3.invokeProperty);
			//globalCollCheckProp3.addEventListener(globalCollCheckProp3.getPropertyID(), cntBubbleMergeProp3.invokePropertyFromEvent);
			cntBubbleMergeProp3.setEnvObj(cntBubble3);	
			cntBubbleMergeProp3.activate();

			//Generate Destroyable
			//----------------------
			var bubblePop1:BasicBubblePop = new BasicBubblePop();
			bubblePop1.setEnvObj(cntBubble1);
			bubblePop1.activate();
			
			var bubblePop2:BasicBubblePop = new BasicBubblePop();
			bubblePop2.setEnvObj(cntBubble2);
			bubblePop2.activate();
			
			var bubblePop3:BasicBubblePop = new BasicBubblePop();
			bubblePop3.setEnvObj(cntBubble3);
			bubblePop3.activate();
			
			//Chain the Destroyable after the Mergable
			cntBubbleMergeProp1.setNextInChain(bubblePop1);
			cntBubbleMergeProp2.setNextInChain(bubblePop2);
			cntBubbleMergeProp3.setNextInChain(bubblePop3);
			
		}
		
		private function initBubblesOnScreen():void {
			
		}
		
		//Adds bubble to main sprite
		private function addBubbleToMainScreen():EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(0x00FFFF,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		private function addCounterBubbleToMainScreen(color:uint):EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(color,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		

		
		
	}

}