/*
 * //TODO Testother feature of the GroupProperties 
 */
package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GlobalEnvObjTable;
	import bb.engine.env.components.GroupedProperties;
	
	import bb.engine.env.components.properties.Collisionable;
	import bb.engine.env.components.properties.GlobalCollisionCheck;
	import bb.game.components.properties.CounterBubbleMerge;
	import bb.engine.env.components.properties.Touchable;	
	import bb.engine.env.components.properties.MouseClicked;
	import bb.game.components.properties.BasicBubblePop;
	
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching 
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestGroupedProperties extends Sprite implements ITest 
	{
		
		public function TestGroupedProperties(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "TestGroupProperties";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			
			var cntBubble1:EnvObj = addCounterBubbleToMainScreen(0x00FF00);
			cntBubble1.x = 415;
			cntBubble1.y = 445;

			//Register counter bubbles to global on screen 
			var globalTable:GlobalEnvObjTable = GlobalEnvObjTable.getInstance();
			var globalCntBubblesID = "CounterBubbles";
			
			//Register bubbles to globalTable
			globalTable.addEnvObj(globalCntBubblesID, cntBubble1);
			
			//Make the Grouped Properties
			var gp:GroupedProperties = new GroupedProperties("Wire Counter Bubble Merge Then Pop");
			
			//Set counter bubble as target
			gp.setTargetEnvObj(cntBubble1);
			
			//Conifg a Touchable MouseClick to Bubble
			//------------------------------------------
			gp.addProperty(new MouseClicked());
			
			//Generate Global Collision Check Handler aggregated
			//------------------------------------------------------
			var globalCollCheckProp1:GlobalCollisionCheck = new GlobalCollisionCheck(globalCntBubblesID);
			globalCollCheckProp1.setEnvObjCollectionID(globalCntBubblesID);

			gp.addProperty(globalCollCheckProp1, false);	//not autowired
			
			//Wire CounterBubbleMerge To a mouse click and aggregate a Collisionable checker
			//-------------------------------------------------------------------------------
			var cntBubbleMergeProp1:CounterBubbleMerge = new CounterBubbleMerge(globalCollCheckProp1);	//create merge prop
			cntBubbleMergeProp1.setCollisionCheckHandler(globalCollCheckProp1);

			gp.addProperty(cntBubbleMergeProp1);	//auto wired to last auto wired property entered
			
			//Generate Destroyable
			//----------------------
			var bubblePop1:BasicBubblePop = new BasicBubblePop();
			cntBubbleMergeProp1.setNextInChain(bubblePop1);//Chain the Destroyable after the Mergable
	
			gp.addProperty(bubblePop1,false);
			
			//activate the group of properties
			gp.activate();
			
			//destroy group of properties
			//gp.destroy();
		}
		
		private function addCounterBubbleToMainScreen(color:uint):EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(color,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
	}

}