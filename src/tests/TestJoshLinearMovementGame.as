package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.engine.env.components.properties.LinearMovement;
	import flash.events.MouseEvent;
	//import fl.controls.TextArea;
	//import fl.controls.TextInput;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class TestJoshLinearMovementGame extends Sprite implements ITest 
	{
		var angle:int;
		var mag:int;
		var angH:int;
		var magH:int;
		var linMove:LinearMovement = new LinearMovement();
		var txt:TextField = new TextField();
		var txt2:TextField = new TextField();
		var resetB:Sprite = new Sprite();
		var enterB:Sprite = new Sprite();
		var posx:int = 400;
		var posy:int = 300;
		
		var leftSide:Sprite = new Sprite();
		var rightSide:Sprite = new Sprite();
		var bottomSide:Sprite = new Sprite();
		var upSide:Sprite = new Sprite();
		var hole:Sprite = new Sprite();
		var bubble:EnvObj = addBubbleToMainScreen();
		var tryUp:Sprite = new Sprite();
		var tryDo:Sprite = new Sprite();
		var tryLe:Sprite = new Sprite();
		var tryRi:Sprite = new Sprite();
		
		var difUp:Sprite = new Sprite();
		var difDo:Sprite = new Sprite();
		
		public function TestLinearMovement(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "LINEARMOVEMENT"
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Add a bubble to the main screen and retrieve it
			//var bubble:EnvObj = addBubbleToMainScreen();

			//Pass in data to the linear movement class and 
			//var linMove:LinearMovement = new LinearMovement();
			linMove.setEnvObj(bubble);
			//linMove.setProjection(angle, mag);
			linMove.setPosition(new Point(posx, posy));
			
			linMove.activate();
			
			//textbox angle
			//var txt:TextField = new TextField();
			var txt3:TextField = new TextField();
			txt3.text = "Enter Angle";
			txt3.x = 15;
			addChild(txt3);
			
			txt.type = "input";
			txt.text = "0";
			txt.border = true;
			txt.borderColor = 0x000000;
			txt.y = 20;
			txt.x = 15;
			addChild(txt);
			//angH = parseInt(txt.text);
			//txt.addEventListener(TextEvent.TEXT_INPUT, changeAngle);
			
			//textbox mag
			//var txt2:TextField = new TextField();
			var txt4:TextField = new TextField();
			txt4.text = "Enter Magnitude";
			txt4.y = 120;
			txt4.x = 15;
			addChild(txt4);
			
			txt2.type = "input";
			txt2.text = "0";
			txt2.border = true;
			txt2.borderColor = 0x000000;
			txt2.y = 140;
			txt2.x = 15;
			addChild(txt2);
			//magH = parseInt(txt2.text);
			//txt2.addEventListener(TextEvent.TEXT_INPUT, changeMag);
			//rest button
			var txt5:TextField = new TextField();
			txt5.text = "Reset";
			txt5.y = 250;
			txt5.x = 15;
			addChild(txt5);
			
			var txt6:TextField = new TextField();
			txt6.text = "Enter";
			txt6.x = 80;
			txt6.y = 250;
			addChild(txt6);
			
			
			resetB.graphics.beginFill(0x000000, 1);
			resetB.graphics.drawCircle(25, 300, 20);
			resetB.graphics.endFill();
			addChild(resetB);
			
			enterB.graphics.beginFill(0x00FF00, 1);
			enterB.graphics.drawCircle(90, 300, 20);
			enterB.graphics.endFill();
			addChild(enterB);
			
			resetB.addEventListener(MouseEvent.CLICK, clicked)
			
			trace(angH);
			trace(magH);
			
			enterB.addEventListener(MouseEvent.CLICK, enterV);
			
			hole.graphics.beginFill(0x000000, 1);
			hole.graphics.drawCircle(700, 400, 50);
			hole.graphics.endFill();
			addChild(hole);
			
			bubble.addEventListener(Event.ENTER_FRAME, collide);
			
			tryUp.graphics.beginFill(0xFF0000, 1);
			tryUp.graphics.drawCircle(75, 450, 20);
			tryUp.graphics.endFill();
			addChild(tryUp);
			
			tryDo.graphics.beginFill(0xFF0000, 1);
			tryDo.graphics.drawCircle(75, 500, 20);
			tryDo.graphics.endFill();
			addChild(tryDo);
			
			tryLe.graphics.beginFill(0xFF0000, 1);
			tryLe.graphics.drawCircle(30, 500, 20);
			tryLe.graphics.endFill();
			addChild(tryLe);
			
			tryRi.graphics.beginFill(0xFF0000, 1);
			tryRi.graphics.drawCircle(120, 500, 20);
			tryRi.graphics.endFill();
			addChild(tryRi);
			
			difUp.graphics.beginFill(0x3F0000, 1);
			difUp.graphics.drawCircle(25, 350, 20);
			difUp.graphics.endFill();
			addChild(difUp);
			
			difDo.graphics.beginFill(0x0000FF, 1);
			difDo.graphics.drawCircle(25, 400, 20);
			difDo.graphics.endFill();
			addChild(difDo);

			tryUp.addEventListener(MouseEvent.CLICK, moveUp);
			tryDo.addEventListener(MouseEvent.CLICK, moveDo);
			tryRi.addEventListener(MouseEvent.CLICK, moveRi);
			tryLe.addEventListener(MouseEvent.CLICK, moveLe);
			difUp.addEventListener(MouseEvent.CLICK, difUp1);
			difDo.addEventListener(MouseEvent.CLICK, difDo1);
		}
		
		
		private function getBorders():Array {
			var borders:Array = new Array();
			
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(0x000000, 1.0);
			upSide.graphics.drawRect(0, 0, 800, 7);
			upSide.graphics.endFill();
			addChild(upSide);
			borders.push(upSide);
			
			var leftSide:EnvObj = new EnvObj();
			leftSide.graphics.beginFill(0x000000, 1.0);
			leftSide.graphics.drawRect(200, 0, 7, 800);
			leftSide.graphics.endFill();
			addChild(leftSide);
			borders.push(leftSide);
			
			var rightSide:EnvObj = new EnvObj();
			rightSide.graphics.beginFill(0x000000, 1.0);
			rightSide.graphics.drawRect(793, 0, 7, 800);
			rightSide.graphics.endFill();
			addChild(rightSide);
			borders.push(rightSide);
			
			var bottomSide:EnvObj = new EnvObj();
			bottomSide.graphics.beginFill(0x000000, 1.0);
			bottomSide.graphics.drawRect(0, 594, 800, 7);
			bottomSide.graphics.endFill();
			addChild(bottomSide);
			borders.push(bottomSide);
			
			return borders;
		}
		
		//Adds bubble to main sprite
		private function addBubbleToMainScreen():EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(0x00FFFF,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		private function changeAngle():void
		{
			angH = parseInt(txt.text);
			angle = angH;
			trace(angle);
			linMove.setProjection(angle, mag);
			trace("call me");
		}
		
		private function changeMag():void
		{
			magH = parseInt(txt2.text);
			mag = magH;
			linMove.setProjection(angle, mag);
		}
		
		private function clicked(event:MouseEvent):void
		{
			angle = 0;
			mag = 0;
			linMove.setProjection(angle, mag);
			linMove.setPosition(new Point(posx, posy));
		}
		
		private function enterV(event:MouseEvent):void
		{
			changeAngle();
			changeMag();
		}
		
		function collide (event:Event):void
		{
			if (bubble.hitTestObject(hole))
			{
				trace("Winner");
				angle = 0;
				mag = 0;
				linMove.setProjection(angle, mag);
				linMove.setPosition(new Point(posx, posy));
				hole.height -= 10;
				hole.width -= 10;
			}
		}
		
			function moveUp (event:Event):void
			{
				angle = 0;
				linMove.setProjection(angle, mag);
			}
			
			function moveDo (event:Event):void
			{
				angle = 180;
				linMove.setProjection(angle, mag);
			}
			
			function moveLe (event:Event):void
			{
				angle = 270;
				linMove.setProjection(angle, mag);
			}
			
			function moveRi (event:Event):void
			{
				angle = 90;
				linMove.setProjection(angle, mag);
			}
		
			function difUp1 (event:Event):void
			{
				mag += 1;
				linMove.setProjection(angle, mag);
			}
			
			function difDo1 (event:Event):void
			{
				mag -= 1;
				linMove.setProjection(angle, mag);
			}
		
	}

}