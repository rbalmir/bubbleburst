package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.properties.Clockable;
	import bb.game.components.properties.BubbleStageBorderCollision;
	import bb.engine.env.components.properties.LinearMovement;
	import bb.engine.env.components.properties.EnterFrameClock;
	
	import flash.events.MouseEvent;
	//import fl.controls.TextArea;
	//import fl.controls.TextInput;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class TestLinearMovement extends Sprite implements ITest 
	{
		var angle:int;
		var mag:int;
		var angH:int;
		var magH:int;
		var linMove:LinearMovement = new LinearMovement();
		var txt:TextField = new TextField();
		var txt2:TextField = new TextField();
		var resetB:Sprite = new Sprite();
		var enterB:Sprite = new Sprite();
		var posx:int = 400;
		var posy:int = 300;
		
		var leftSide:Sprite = new Sprite();
		var rightSide:Sprite = new Sprite();
		var bottomSide:Sprite = new Sprite();
		var upSide:Sprite = new Sprite();
		var hole:Sprite = new Sprite();
		var bubble:EnvObj;
		
		var txtInputAngle:TextField;
		var txtInputMag:TextField;
		
		public function TestLinearMovement(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "LINEARMOVEMENT"
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Add a bubble to the main screen and retrieve it
			var bubble:EnvObj = addBubbleToMainScreen();
			bubble.x = 415;
			bubble.y = 415;
			
			
			//Pass in data to the linear movement class and 
			var linMove:LinearMovement = new LinearMovement();
			linMove.setEnvObj(bubble);
			//linMove.setProjection(angle, mag);
			linMove.setPosition(new Point(415, 415));
			linMove.activate();
			
			//Wire the linearMovement to a Clock
			var clock:Clockable = new EnterFrameClock();
			clock.addEventListener(clock.getPropertyID(), linMove.invokeProperty);
			clock.activate();
			
			//assign to class variable
			this.linMove = linMove;
			
			//Set up text boxes
			this.setUpTextBoxes();
			
			//magH = parseInt(txt2.text);
			//txt2.addEventListener(TextEvent.TEXT_INPUT, changeMag);
			//rest button
			var txt5:TextField = new TextField();
			txt5.text = "Reset";
			txt5.y = 250;
			txt5.x = 15;
			addChild(txt5);
			
			var txt6:TextField = new TextField();
			txt6.text = "Enter";
			txt6.x = 80;
			txt6.y = 250;
			addChild(txt6);
			
			//reset demo
			resetB.graphics.beginFill(0x000000, 1);
			resetB.graphics.drawCircle(25, 300, 20);
			resetB.graphics.endFill();
			addChild(resetB);
			
			//
			enterB.graphics.beginFill(0x00FF00, 1);
			enterB.graphics.drawCircle(90, 300, 20);
			enterB.graphics.endFill();
			addChild(enterB);
			
			resetB.addEventListener(MouseEvent.CLICK, this.reset);
			enterB.addEventListener(MouseEvent.CLICK, this.setProjectionValues);			
		}
		
		//Adds bubble to main sprite
		private function addBubbleToMainScreen():EnvObj
		{
			var bubble:EnvObj = new EnvObj();
			bubble.graphics.beginFill(0x00FFFF,1);
			bubble.graphics.drawCircle(0, 0, 30);
			bubble.graphics.endFill();
			this.addChild(bubble);
		
			return bubble;
		}
		
		private function setUpTextBoxes() {
							//Angle Label
			var lblEnterAngle:TextField = new TextField();
			lblEnterAngle.text = "New Enter Angle";
			lblEnterAngle.selectable = false;
			lblEnterAngle.x = 0;
			addChild(lblEnterAngle);
			
			//TextBox To Enter Angle 
			var txtInputAngle:TextField = new TextField();
			txtInputAngle.type = "input";
			txtInputAngle.text = "0";
			txtInputAngle.border = true;
			txtInputAngle.borderColor = 0x000000;
			txtInputAngle.y = 20;
			txtInputAngle.x = 0;
			addChild(txtInputAngle);
			
			//Mag Label
			var lblMag:TextField = new TextField();
			lblMag.text = "New Enter Mag";
			lblMag.selectable = false;
			lblMag.x = 1;
			lblMag.y = 40
			addChild(lblMag);
			
			//TextBox To Enter Angle 
			var txtInputMag:TextField = new TextField();
			txtInputMag.type = "input";
			txtInputMag.text = "0";
			txtInputMag.border = true;
			txtInputMag.borderColor = 0x000000;
			txtInputMag.y = 60;
			txtInputMag.x = 0;
			addChild(txtInputMag);
			
			this.txtInputAngle = txtInputAngle;
			this.txtInputMag = txtInputMag;
		
		}
		
		private function changeAngle():void
		{
			angH = parseInt(txt.text);
			angle = angH;
			trace(angle);
			
			
			linMove.setProjection(angle, mag);
			trace("call me");
		}
		
		private function changeMag():void
		{
			magH = parseInt(txt2.text);
			mag = magH;
			linMove.setProjection(angle, mag);
		}
		
		private function clicked(event:MouseEvent):void
		{
			angle = 0;
			mag = 0;
			linMove.setProjection(angle, mag);
			//linMove.setPosition(new Point(posx, posy));
			linMove.setPosition(new Point(415, 415));
		}
		
		private function reset(event:MouseEvent):void
		{
			var angle:Number = 0;
			var mag:Number = 0;
			linMove.setProjection(angle, mag);
			//linMove.setPosition(new Point(posx, posy));
			linMove.setPosition(new Point(415, 415));
		}
		
		private function setProjectionValues(event:MouseEvent):void{
			var ang:Number = parseInt(txtInputAngle.text);
			var mag:Number = parseInt(txtInputMag.text);
			
			linMove.setProjection(ang, mag);
		}
		
		private function enterV(event:MouseEvent):void
		{
			changeAngle();
			changeMag();			
		}
		
	}

}