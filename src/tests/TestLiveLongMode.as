package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.game.components.*;
	import flash.display.Sprite;
	import bb.game.mode.LiveLong.LiveLongMode;
	import bb.game.*;
	
	
	/**
	 * Test the live long mode.
	 * @author Rich Balmir
	 */
	public class TestLiveLongMode extends Sprite implements ITest 
	{
		var baseSprite:Sprite;
		
		public function TestLiveLongMode(baseSprite:Sprite) 
		{
			this.baseSprite = baseSprite;
			//baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "TestLiveLongMode";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			AppConfig.setRootLayer(baseSprite);
			
			//
			/*
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(0x000000, 1.0);
			upSide.graphics.drawRect(0, 0, 800, 7);
			upSide.graphics.endFill();
			baseSprite.addChild(upSide);
			*/
			//
			new LiveLongMode();
		}
		
		private function getBorders():Array
		{
			var borders:Array = new Array();
			
			var upSide:EnvObj = new EnvObj();
			upSide.graphics.beginFill(0x000000, 1.0);
			upSide.graphics.drawRect(0, 0, 800, 7);
			upSide.graphics.endFill();
			addChild(upSide);
			borders.push(upSide);
			
			var leftSide:EnvObj = new EnvObj();
			leftSide.graphics.beginFill(0x000000, 1.0);
			leftSide.graphics.drawRect(200, 0, 7, 800);
			leftSide.graphics.endFill();
			addChild(leftSide);
			borders.push(leftSide);
			
			var rightSide:EnvObj = new EnvObj();
			rightSide.graphics.beginFill(0x000000, 1.0);
			rightSide.graphics.drawRect(793, 0, 7, 800);
			rightSide.graphics.endFill();
			addChild(rightSide);
			borders.push(rightSide);
			
			var bottomSide:EnvObj = new EnvObj();
			bottomSide.graphics.beginFill(0x000000, 1.0);
			bottomSide.graphics.drawRect(0, 594, 800, 7);
			bottomSide.graphics.endFill();
			addChild(bottomSide);
			borders.push(bottomSide);
			
			return borders;
		}
		
	}

}