package tests 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.GlobalEnvObjTable;
	import bb.game.components.properties.OmniBurst;
	import tests.components.OmniBurstBubble;
	
	import bb.engine.env.components.properties.Collisionable;
	import bb.engine.env.components.properties.GlobalCollisionCheck;
	import bb.game.components.properties.CounterBubbleMerge;
	import bb.engine.env.components.properties.Touchable;	
	import bb.engine.env.components.properties.MouseClicked;
	import bb.game.components.properties.BasicBubblePop;
	
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.events.TextEvent;
	import flash.display.Stage;
	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching 
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestOmniBurst extends Sprite implements ITest 
	{
		
		public function TestOmniBurst(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "OmniBurst";
		}
		
		public function testDescription():String 
		{
			return "";
		}
		
		public function test():void 
		{
			//Init regular and counter bubbles
			var omniBubble:EnvObj = new OmniBurstBubble();
			
			omniBubble.x = 415;
			omniBubble.y = 415;
			
			addChild(omniBubble);
			
			
		}
			
	}

}