package tests 
{
	import bb.engine.env.components.*;
	import bb.engine.env.components.properties.*;
	import bb.game.components.*;
	import flash.display.Sprite;

	/**
	 * Test the Merging of Counter Bubbles and following a Bubble Pop on consective touching 
	 * of the bubble.
	 * @author Rich Balmir
	 */
	public class TestRegularBubble extends Sprite implements ITest 
	{
		
		public function TestRegularBubble(baseSprite:Sprite) 
		{
			baseSprite.addChild(this);
		}
		
		/* INTERFACE tests.ITest */
		
		public function testName():String 
		{
			return "RegularBubble";
		}
		
		public function testDescription():String 
		{
			return "Create Regular Bubble and push in direction";
		}
		
		public function test():void 
		{
			//Init regular and counter bubbles
			var regularBubble:RegularBubble = new RegularBubble();
			regularBubble.x = 300;
			regularBubble.y = 300;
			
			var iProject:IProjectable = regularBubble.getIProjectable();
			iProject.setProjection(45, 1);
			
			addChild(regularBubble);	
		}
			
	}

}