package tests.components 
{
	import bb.engine.env.components.EnvObj;
	import bb.engine.env.components.ISupportProjectable;
	import bb.engine.env.components.properties.Clockable;
	import bb.engine.env.components.properties.EnterFrameClock;
	import bb.engine.env.components.properties.IProjectable;
	import bb.engine.env.components.properties.LinearMovement;
	import bb.engine.env.components.properties.MouseClicked;
	import bb.engine.env.components.properties.Touchable;
	import bb.game.components.properties.OmniBurst;
	import bb.game.components.Bubble;
	/**
	 * ...
	 * @author Rich Balmir
	 */
	public class OmniBurstBubble extends Bubble implements ISupportProjectable 
	{
		//private var iProject:IProjectable = null;
		
		public function OmniBurstBubble() 
		{
			super("OmniBurstBubble");
			
			init();
		}
		
		/* INTERFACE bb.engine.env.components.ISupportProjectable */
		/*
		public function getIProjectable():IProjectable 
		{
			return iProject;
		}
		*/
		private function init():void {
			addTestBubble(0x333333);
			
			configProperties();
		}
		
		//Configure the properties that make a CounterBubble
		protected function configProperties():void {
			//1)Handles mouse click, 
			//3)omniburst into regular bubbles
			//---------------------------------------
			//Conifg a Touchable MouseClick to Bubble
			//------------------------------------------
 			var bubClick:MouseClicked = new MouseClicked();
			bubClick.setEnvObj(this);
						
			var bubBurst:OmniBurst = new OmniBurst();
			bubBurst.setBurstCount(10);
			bubBurst.setEnvObj(this);
			bubClick.addEventListener(bubClick.getPropertyID(), bubBurst.invokeProperty);
			
			bubClick.activate();
			bubBurst.activate();
			
			//Clockable to Projectable
			//--------------------------------------------
			var clock:Clockable = new EnterFrameClock();
			
			var linMove:LinearMovement = new LinearMovement();
			linMove.setEnvObj(this);
			clock.addEventListener(clock.getPropertyID(), linMove.invokeProperty);
			
			iProject = linMove;
			
			clock.activate();
			linMove.activate();
		}
		
		override public function clone():EnvObj{
			return new OmniBurstBubble();
		}
	}

}